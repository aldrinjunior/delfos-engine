package com.delfos;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelfosEngineApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(DelfosEngineApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }

}
