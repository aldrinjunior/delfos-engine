package com.delfos.resource

import com.delfos.domain.ContactGroup
import com.delfos.service.ContactGroupService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/contactgroups")
class ContactGroupResource : AbstractResource<ContactGroupService, ContactGroup, String>() {

    @Autowired
    private lateinit var contactGroupService: ContactGroupService

    override fun getService(): ContactGroupService {
        return contactGroupService
    }

}
