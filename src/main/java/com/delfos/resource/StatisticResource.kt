package com.delfos.resource

import com.delfos.domain.EventType
import com.delfos.domain.view.CountView
import com.delfos.domain.view.EventLogStatisticView
import com.delfos.service.StatisticService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/statistic")
class StatisticResource @Autowired
constructor(private val statisticService: StatisticService) {

    @GetMapping("/summary")
    fun summary(): ResponseEntity<List<CountView>> {
        val list = statisticService.summary()
        return ResponseEntity.ok(list)
    }

    @GetMapping("/count/event-type")
    fun countEventLogByEventType(): ResponseEntity<List<EventLogStatisticView>> {
        val list = statisticService.countEventLogByEventType()
        return ResponseEntity.ok(list)
    }

    @GetMapping("/count/event")
    fun countEventLogByEvent(
            @RequestParam(name = "type", required = false) eventType: EventType?): ResponseEntity<List<EventLogStatisticView>> {
        val list = statisticService.countEventLogByEvent(eventType).orEmpty()
        return ResponseEntity.ok(list)
    }

    @GetMapping("/count/date/event")
    fun countEventLogByDateHourAndEvent(): ResponseEntity<List<EventLogStatisticView>> {
        val list = statisticService.countEventLogByDateHourAndEvent().orEmpty()
        return ResponseEntity.ok(list)
    }

    @GetMapping("/count/event/event-type-error")
    fun countEventLogByEventAndEventTypeError(): ResponseEntity<List<EventLogStatisticView>> {
        val list = statisticService.countEventLogByEventAndEventTypeError()
        return ResponseEntity.ok(list)
    }

}
