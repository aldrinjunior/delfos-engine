package com.delfos.resource

import com.delfos.domain.MessageLog
import com.delfos.domain.filter.MessageLogFilter
import com.delfos.service.MessageLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/message/logs")
class MessageLogResource @Autowired
constructor(private val messageLogService: MessageLogService) {

    @GetMapping("/{id}")
    fun find(@PathVariable id: String): ResponseEntity<MessageLog> {
        val messageLog = messageLogService.findById(id)
        return ResponseEntity.ok().body(messageLog)
    }

    @PostMapping("/find")
    fun find(@RequestBody filterBy: MessageLogFilter,
             @PageableDefault pageable: Pageable): ResponseEntity<Page<MessageLog>> {
        val page = messageLogService.find(filterBy, pageable)
        return ResponseEntity.ok(page)

    }

}
