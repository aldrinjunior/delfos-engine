package com.delfos.resource

import com.delfos.domain.Component
import com.delfos.service.ComponentService
import com.eoscode.springapitools.resource.AbstractResource
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/components")
class ComponentResource @Autowired
constructor(private val componentService: ComponentService) : AbstractResource<ComponentService, Component, String>() {

    override fun getService(): ComponentService {
        return componentService
    }

    @GetMapping("/{id}/token")
    fun findTokenById(@PathVariable id: String): ResponseEntity<String> {

        val component = componentService.findById(id)

        val mapper = ObjectMapper()
        val rootNode = mapper.createObjectNode()
        rootNode.put("id", id)
        rootNode.put("token", component.token!!)
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(rootNode.toString())

    }

    @GetMapping("/{id}/reset/token")
    fun resetToken(@PathVariable id: String): ResponseEntity<String> {

        componentService.resetToken(id)
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .contentType(MediaType.APPLICATION_JSON)
                .build()

    }

    @GetMapping("/{id}/token/signature")
    fun signatureToken(@PathVariable id: String): ResponseEntity<String> {

        val component = componentService.findById(id)

        val mapper = ObjectMapper()
        val rootNode = mapper.createObjectNode()
        rootNode.put("id", id)
        rootNode.put("token", componentService.generateSignatureToken(component.token!!))
        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(rootNode.toString())

    }


}
