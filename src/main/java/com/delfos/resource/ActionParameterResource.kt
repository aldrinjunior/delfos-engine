package com.delfos.resource

import com.delfos.domain.ActionParameter
import com.delfos.service.ActionParameterService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/actionparameters")
class ActionParameterResource : AbstractResource<ActionParameterService, ActionParameter, String>() {

    @Autowired
    private lateinit var actionParameterService: ActionParameterService


    override fun getService(): ActionParameterService {
        return actionParameterService
    }

}
