package com.delfos.resource

import com.delfos.domain.Component
import com.delfos.engine.ApplicationEngine
import com.delfos.engine.message.MessagePayload
import com.delfos.service.ComponentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import javax.servlet.http.HttpServletRequest
import java.util.Date

@RestController
@RequestMapping("/api/analyzer")
class MessageAnalyzerResource @Autowired
constructor(private val applicationEngine: ApplicationEngine, private val componentService: ComponentService) {

    @PostMapping
    fun analyze(@RequestBody payload: MessagePayload,
                request: HttpServletRequest): ResponseEntity<*> {

        val header = request.getHeader("Authorization")
        if (header != null && header.startsWith("Bearer ")) {
            val component = findBySignatureToken(header.substring(7))
            if (component != null) {
                applicationEngine.analyze(payload, component)
                return ResponseEntity.ok().build<Any>()
            }
        }

        return ResponseEntity.status(401).body(json())

    }

    private fun findBySignatureToken(token: String): Component? {
        return if (componentService.isSignatureValid(token)) {
            componentService.findBySignatureToken(token)
        } else null
    }

    private fun json(): String {
        val date = Date().time
        return ("{\"timestamp\": " + date + ", "
                + "\"status\": 401, "
                + "\"error\": \"not authorized\", "
                + "\"message\": \"component not found\", "
                + "\"path\": \"/api/analyzer\"}")
    }

}
