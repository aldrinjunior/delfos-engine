package com.delfos.resource

import com.delfos.domain.filter.MessageLogStatisticFilter
import com.delfos.domain.view.MessageLogStatisticView
import com.delfos.service.MessageLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.query.Param
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import java.util.Date

@RestController
@RequestMapping("/api/message/logs/statistic")
class MessageLogStatisticResource @Autowired
constructor(private val messageLogService: MessageLogService) {

    @PostMapping
    fun statistic(@RequestBody statisticBy: MessageLogStatisticFilter): ResponseEntity<List<MessageLogStatisticView>> {
        val view = messageLogService.statisticQuery(statisticBy)
        return ResponseEntity.ok(view)
    }

    @GetMapping("/date")
    fun statisticByDate(
            @Param("dateHour") @DateTimeFormat(pattern = "yyyy-MM-dd") dateHour: Date?,
            @Param("dateHourEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") dateHourEnd: Date?,
            @Param("groups") groups: List<String>?): ResponseEntity<List<MessageLogStatisticView>> {

        val view = messageLogService.statisticQueryByDate(
                dateHour,
                dateHourEnd,
                groups)

        return ResponseEntity.ok(view)
    }

}
