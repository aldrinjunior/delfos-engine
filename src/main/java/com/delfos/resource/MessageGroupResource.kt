package com.delfos.resource

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import com.delfos.domain.MessageGroup
import com.delfos.service.MessageGroupService
import com.eoscode.springapitools.resource.AbstractResource

@RestController
@RequestMapping("/api/messagegroups")
class MessageGroupResource : AbstractResource<MessageGroupService, MessageGroup, String>() {

    @Autowired
    private lateinit var messageGroupService: MessageGroupService

    override fun getService(): MessageGroupService {
        return messageGroupService
    }

}
