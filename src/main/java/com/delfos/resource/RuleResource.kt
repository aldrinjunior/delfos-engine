package com.delfos.resource

import com.delfos.domain.Rule
import com.delfos.service.RuleService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/rules")
class RuleResource : AbstractResource<RuleService, Rule, String>() {

    @Autowired
    private lateinit var ruleService: RuleService

    override fun getService(): RuleService {
        return ruleService
    }

    @GetMapping("/search")
    fun search(@RequestParam("name") name: String): ResponseEntity<List<Rule>> {
        val list = service.findByNameOrDescription(name)
        return ResponseEntity.ok(list)
    }

}
