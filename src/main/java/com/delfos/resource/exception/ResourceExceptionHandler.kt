package com.delfos.resource.exception

import com.delfos.engine.exception.ValidationRuleException
import com.eoscode.springapitools.resource.exception.BaseResourceExceptionHandler
import com.eoscode.springapitools.resource.exception.StandardError
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

import javax.servlet.http.HttpServletRequest

@RestControllerAdvice
class ResourceExceptionHandler : BaseResourceExceptionHandler() {

    @ExceptionHandler(ValidationRuleException::class)
    fun validationRule(e: ValidationRuleException, request: HttpServletRequest): ResponseEntity<StandardError> {

        val err = StandardError(System.currentTimeMillis(), HttpStatus.BAD_REQUEST.value(),
                "Invalid rule", e.message, request.requestURI)
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err)
    }

}
