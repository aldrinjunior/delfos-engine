package com.delfos.resource

import com.delfos.domain.Action
import com.delfos.service.ActionService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/actions")
class ActionResource : AbstractResource<ActionService, Action, String>() {

    @Autowired
    private lateinit var actionService: ActionService

    override fun getService(): ActionService {
        return actionService
    }

}
