package com.delfos.resource

import com.delfos.domain.EventLog
import com.delfos.domain.filter.EventLogFilter
import com.delfos.service.EventLogService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/event/logs")
class EventLogResource @Autowired
constructor(private val eventLogService: EventLogService) {

    @GetMapping("/{id}")
    fun find(@PathVariable id: String): ResponseEntity<EventLog> {
        val eventLog = eventLogService.findById(id)
        return ResponseEntity.ok().body(eventLog)
    }

    @PostMapping("/find")
    fun find(@RequestBody filterBy: EventLogFilter,
             @PageableDefault pageable: Pageable): ResponseEntity<Page<EventLog>> {
        val page = eventLogService.find(filterBy, pageable)
        return ResponseEntity.ok(page)
    }

}
