package com.delfos.resource

import com.delfos.domain.Contact
import com.delfos.service.ContactService
import com.eoscode.springapitools.resource.AbstractResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/contacts")
class ContactResource : AbstractResource<ContactService, Contact, String>() {

    @Autowired
    private lateinit var contactService: ContactService

    override fun getService(): ContactService {
        return contactService
    }

}
