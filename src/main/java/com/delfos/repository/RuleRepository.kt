package com.delfos.repository

import com.delfos.domain.Rule
import com.eoscode.springapitools.data.repository.Repository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

@org.springframework.stereotype.Repository
interface RuleRepository : Repository<Rule, String> {

    //public List<Rule> findAllByTypeAndConditionsIsNotNullAndConditions

    @Query("select distinct rule from Rule rule left join fetch  rule.conditions condiction left join fetch condiction.field" + " where condiction.parent = null and rule.state = 1 order by rule.lastModifiedDate")
    fun findAllEnabled(): List<Rule>

    @Query("select distinct rule from Rule rule left join fetch rule.conditions condiction left join fetch condiction.field" + " where rule.id = :id and condiction.parent = null")
    fun loadRule(@Param("id") id: String): Rule

    fun findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseOrderByName(name: String, description: String): List<Rule>

}
