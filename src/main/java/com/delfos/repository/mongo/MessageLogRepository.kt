package com.delfos.repository.mongo

import com.delfos.domain.MessageLog
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface MessageLogRepository : MongoRepository<MessageLog, String>
