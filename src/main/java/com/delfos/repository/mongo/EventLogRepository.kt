package com.delfos.repository.mongo

import com.delfos.domain.EventLog
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface EventLogRepository : MongoRepository<EventLog, String>
