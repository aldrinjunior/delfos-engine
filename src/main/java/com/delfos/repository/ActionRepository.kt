package com.delfos.repository

import com.delfos.domain.Action
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface ActionRepository : Repository<Action, String>
