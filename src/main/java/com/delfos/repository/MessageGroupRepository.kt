package com.delfos.repository

import com.delfos.domain.MessageGroup
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface MessageGroupRepository : Repository<MessageGroup, String>
