package com.delfos.repository

import com.delfos.domain.Condition
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface ConditionRepository : Repository<Condition, String>
