package com.delfos.repository

import com.delfos.domain.ActionParameter
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface ActionParameterRepository : Repository<ActionParameter, String>
