package com.delfos.repository

import com.delfos.domain.Component
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface ComponentRepository : Repository<Component, String> {

    fun findByToken(token: String): Component

}
