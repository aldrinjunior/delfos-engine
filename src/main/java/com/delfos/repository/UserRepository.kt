package com.delfos.repository

import com.delfos.domain.User
import com.eoscode.springapitools.data.repository.Repository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

import java.util.Optional

@org.springframework.stereotype.Repository
interface UserRepository : Repository<User, String> {

    @Query("select user from User user where user.username = :username and user.status = 1")
    fun findUserByUsername(@Param("username") username: String): Optional<User>

}
