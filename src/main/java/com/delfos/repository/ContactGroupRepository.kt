package com.delfos.repository

import com.delfos.domain.ContactGroup
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface ContactGroupRepository : Repository<ContactGroup, String>
