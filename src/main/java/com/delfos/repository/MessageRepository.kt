package com.delfos.repository

import com.delfos.domain.Message
import com.eoscode.springapitools.data.repository.Repository
import org.springframework.data.jpa.repository.Query
import java.util.Optional

@org.springframework.stereotype.Repository
interface MessageRepository : Repository<Message, String> {

    /*@Query("select message from Message message left join fetch message.messageGroup where message.id = :id")
	Optional<Message> findById(String id);*/

    /*@EntityGraph(value = "Message.detail", type = EntityGraphType.LOAD)
	Message findDetailById(String id);*/

    @Query("select message from Message message left outer join fetch message.fields where message.name = :name")
    fun findByName(name: String): Optional<Message>

    fun findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseOrderByName(name: String, description: String): List<Message>

}
