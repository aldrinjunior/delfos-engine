package com.delfos.repository

import com.delfos.domain.Contact
import com.eoscode.springapitools.data.repository.Repository

@org.springframework.stereotype.Repository
interface ContactRepository : Repository<Contact, String>
