package com.delfos.config;

import com.eoscode.springapitools.config.SpringApiToolsScan;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringApiToolsScanConfiguration extends SpringApiToolsScan {}
