package com.delfos.config

import com.delfos.domain.converter.DataFieldLogReadConverter
import com.mongodb.MongoClient
import com.mongodb.MongoClientOptions
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Lazy
import org.springframework.context.annotation.Profile
import org.springframework.core.convert.converter.Converter
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.convert.MongoCustomConversions
import java.util.*

@Configuration
@Profile("test", "dev")
@Lazy
class MongoConfig : AbstractMongoConfiguration() {

    @Value("\${mongodb.hostname}")
    private val hostname: String? = null

    @Value("\${mongodb.port}")
    private val port: Int = 0

    @Value("\${mongodb.username}")
    private val username: String? = null

    @Value("\${mongodb.password}")
    private val password: String? = null

    @Value("\${mongodb.database}")
    private val database: String? = null

    @Profile("test")
    @Bean
    @Throws(Exception::class)
    fun mongoTemplateTest(): MongoTemplate {
        /*IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION)
                .net(new Net(ip, port, Network.localhostIsIPv6()))
                .build();

        MongodStarter starter = MongodStarter.getDefaultInstance();
        MongodExecutable mongodExecutable = starter.prepare(mongodConfig);
        mongodExecutable.start();*/
        return super.mongoTemplate()

    }

    @Profile("dev")
    @Bean
    @Throws(Exception::class)
    override fun mongoTemplate(): MongoTemplate {
        return super.mongoTemplate()
    }

    override fun customConversions(): org.springframework.data.convert.CustomConversions {
        val converterList = ArrayList<Converter<*, *>>()
        converterList.add(DataFieldLogReadConverter())
        return MongoCustomConversions(converterList)
    }

    override fun mongoClient(): MongoClient {

        val serverAddress = ServerAddress(hostname, port)
        return MongoClient(serverAddress,
                MongoCredential.createCredential(username!!, database!!, password!!.toCharArray()),
                MongoClientOptions.builder().build())
    }

    override fun getDatabaseName(): String {
        return "delfos"
    }
}
