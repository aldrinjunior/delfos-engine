package com.delfos.config

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

@Configuration
class JacksonConfig {

    @Bean
    fun mappingJackson2HttpMessageConverter(): MappingJackson2HttpMessageConverter {
        val jsonConverter = MappingJackson2HttpMessageConverter()

        val objectMapper = jsonConverter.objectMapper

        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        objectMapper.setDefaultPropertyInclusion(JsonInclude.Include.NON_EMPTY)

        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)

        /* SimpleModule simpleModule = new SimpleModule()
                .setSerializerModifier(new CustomBeanSerializerModifier())
                .setDeserializerModifier(new CustomBeanDeserializerModifier());
        objectMapper.registerModule(simpleModule);
*/
        /*builder.propertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        builder.indentOutput(true).dateFormat(new SimpleDateFormat("yyyy-MM-dd"));*/

        val hibernate5Module = Hibernate5Module()
        objectMapper.registerModule(hibernate5Module)

        val kotlinModule = KotlinModule()
        objectMapper.registerModule(kotlinModule)

        return jsonConverter
    }

}
