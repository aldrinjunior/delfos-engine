package com.delfos.engine

import com.delfos.domain.*
import com.delfos.engine.action.ExecuteAction
import com.delfos.engine.message.MessageModel
import com.delfos.service.ActionTypeService
import com.delfos.service.EventLogService
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.File
import java.io.Serializable
import java.net.URLClassLoader
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.annotation.PostConstruct

@Service
class EventManager @Autowired
constructor(private val eventLogService: EventLogService,
            private val actionTypeService: ActionTypeService
) : Serializable {

    private val logger = LogFactory.getLog(EventManager::class.java)

    private var executor: Executor? = null

    @PostConstruct
    fun init() {
        executor = Executors.newScheduledThreadPool(40)
    }

    @Synchronized
    private fun createEventLog(event: Event,
                               rule: Rule, messageModel: MessageModel): EventLog {

        val component = messageModel.component
        val eventLog = EventLog()
        eventLog.dateHour = Date()

        eventLog.eventId = event.id
        eventLog.event = Event(event.id!!, event.name!!, event.type!!)
        eventLog.eventType = event.type

        eventLog.ruleId = rule.id
        eventLog.rule = Rule(rule.id!!, rule.name!!)

        eventLog.messageId = messageModel.message.id
        eventLog.message = Message(messageModel.message.id!!, messageModel.name!!)

        eventLog.componentId = component.id
        eventLog.component = Component(component.id!!, component.name!!)

        eventLog.identifier = messageModel.identifier
        eventLog.hash = messageModel.hash

        return eventLog

    }

    private fun prepareDetail(eventLog: EventLog, event: MessageModel) {
        val dataFields = event.fields

        dataFields.forEach { dataField ->
            val logDataField = DataFieldLog()
            logDataField.fieldId = dataField.field.id
            logDataField.name = dataField.name
            logDataField.value = dataField.valueObject
            eventLog.addDataField(logDataField)
        }
    }

    fun execute(events: List<Event>, rule: Rule, messageModel: MessageModel): Int {

        for (event in events) {

            var actionOK = true
            var newEventLog = createEventLog(event, rule, messageModel)
            prepareDetail(newEventLog, messageModel)

            if (event.delay > 0 || event.retry > 0) {

                var eventLog: EventLog? = eventLogService.findByEventAndRule(event, rule,
                        messageModel.identifier,
                        messageModel.hash).orElse(null)

                /*
                 * tratar o tempo em segundos
                 */
                val now = Date()

                val delay = event.delay * 1000
                val interval: Long
                var startDelay: Long = 0
                var count: Long = 0
                var eventStartId: String? = null
                var delayEnd: Date? = null

                if (eventLog != null) {
                    if (eventLog.delayStart != null) {
                        startDelay = eventLog.delayStart!!.time
                    }
                    count = eventLog.count
                    eventStartId = eventLog.id
                    delayEnd = eventLog.delayEnd
                }


                interval = now.time - startDelay

                if (interval > delay * 1.1
                        || event.retry > 0 && count > event.retry
                        || delayEnd != null) {

                    eventStartId = null
                    newEventLog.delayStart = now
                    actionOK = false

                    if (event.retry > 0) {
                        newEventLog.count = 1
                    }

                } else {
                    if (interval < delay || count < event.retry) {
                        actionOK = false
                        if (event.retry > 0) {
                            if (eventLog != null) {
                                eventLog.count = eventLog.count + 1
                                eventLog = eventLogService.save(eventLog)

                                if (eventLog.count > event.retry) {
                                    actionOK = true
                                }
                            } else {
                                newEventLog.count = 1
                            }
                        }
                    }
                }

                newEventLog.eventStartId = eventStartId

                //delay finalize
                if (actionOK && eventLog != null) {
                    eventLog.delayEnd = Date()
                    eventLogService.save(eventLog)
                }
            }

            newEventLog = eventLogService.save(newEventLog)

            if (!actionOK) {
                continue
            }

            if (!event.actions.isNullOrEmpty()) {

                val actions = HashSet(event.actions!!)
                for (action in actions) {
                    val urlClassLoader: URLClassLoader
                    try {
                        var type: Class<*>? = null
                        var actionType = action.actionType

                        if (actionType == null) {
                            actionType = actionTypeService.findById(action.getActionTypeId())
                        }

                        actionType?.apply {
                            if (isSystem) {
                                type = this.javaClass.classLoader.loadClass(actionType.executable)
                            } else {
                                val urlArray = arrayOf(File(actionType.path!!).toURI().toURL())
                                urlClassLoader = URLClassLoader(urlArray, this.javaClass.classLoader)
                                type = urlClassLoader.loadClass(actionType.executable)
                            }
                        }

                        val parametersMap = HashMap<String, String>()
                        if (!action.parameters.isNullOrEmpty()) {
                            action.parameters?.forEach { actionParameter ->
                                parametersMap[actionParameter.type!!.name!!] = actionParameter.value!!
                            }
                        }
                        val parameters = arrayOf(String::class.java, Event::class.java, Rule::class.java, MessageModel::class.java, Map::class.java)

                        type?.let {
                            val constructor = it.getDeclaredConstructor(*parameters)
                            val executable = constructor.newInstance(newEventLog.id, event,
                                    rule, messageModel, parametersMap) as ExecuteAction
                            executor!!.execute(executable)
                        }

                    } catch (e: Exception) {
                        logger.error(e.message, e)
                    }
                }
            }
        }
        return 0
    }

}
