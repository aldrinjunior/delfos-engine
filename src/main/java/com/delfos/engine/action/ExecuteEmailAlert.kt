package com.delfos.engine.action

import com.delfos.domain.Event
import com.delfos.domain.Rule
import com.delfos.engine.message.MessageModel
import java.util.*

class ExecuteEmailAlert(occurrence: String,
                        alert: Event,
                        rule: Rule,
                        messageModel: MessageModel,
                        parameters: Map<String, Any>
) : ExecuteAction(occurrence, alert, rule, messageModel, parameters) {

    public override fun execute()
            /*throws CommandException*/ {

        val authentication = "1" == parameters[PARAM_AUTHENTICATION]
        //boolean authentication = Boolean.parseBoolean(parameters.get(PARAM_AUTHENTICATION));

        val props = Properties()
        props["mail.smtp.host"] = parameters[PARAM_HOST]
        props["mail.smtp.auth"] = java.lang.Boolean.toString(authentication)
        props["mail.smtp.port"] = "587"
        // props.put("mail.debug", "true");
        // props.put("mail.smtp.port", SMTP_PORT);
        // props.put("mail.smtp.socketFactory.port", SMTP_PORT);
        // props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
        // props.put("mail.smtp.socketFactory.fallback", "false");

        try {

            /*// session.setDebug(true);
			Session session = null;
			InternetAddress addressFrom = null;
			if (authentication){
				session = Session.getInstance(props,
						new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(parameters.get(PARAM_USER),
										parameters.get(PARAM_PASSWORD));
							}
						});
				//addressFrom = InternetAddress.getLocalAddress(session);
			}else{
				session = Session.getDefaultInstance(props);
				//addressFrom = new InternetAddress(parameters.get(PARAM_USER));
			}

			addressFrom = new InternetAddress(parameters.get(PARAM_USER));

			Message msg = new MimeMessage(session);
			msg.setFrom(addressFrom);

			// msg.setHeader("Reply-To", "<email@resposta>");

			List<Contact> contacts = getContacts();
			InternetAddress[] addressTo = new InternetAddress[contacts.size()];

			int i = 0;
			for (Contact contact : contacts) {
				addressTo[i] = new InternetAddress(contact.getUsername());
				i++;
			}

			msg.setRecipients(Message.RecipientType.TO, addressTo);

			if (parameters.containsKey(PARAM_TITLE)) {
				msg.setSubject(parameters.get(PARAM_TITLE) + " - " + getOcurrence() + ": "
					+ getEvent().getId());
			} else {
				msg.setSubject(getEvent().getId() + " - " + getOcurrence());
			}

			StringBuilder body = new StringBuilder();

			body.append("<html>");

			body.append("<head>");
			body.append("</head>");

			body.append("<body>");

			body.append("<b>Ocorr�ncia:</b> " + getOcurrence() + "<br/>");

			if (parameters.containsKey(PARAM_MESSAGE)) {
				body.append(parameters.get(PARAM_MESSAGE));
				body.append("<br/>");
				body.append("<br/>");
			}

			if (getEvent().getDescription() != null
					&& getEvent().getDescription().length() > 0) {
				body.append("<b>Mensagem do administrador:</b>");
				body.append("<br/>");
				body.append(getEvent().getDescription());
				body.append("<br/> <br/> ");
			}

			body.append("");
			body.append("<b>Regra aplicada:</b> " + getRule().getId() + "<br/>");
			body.append("");
			body.append("");

			MessageModel message = getMessage();
			body.append("<b>Mensagem de notifica��o:</b> "
					+ message.getMessage().getId() + "<br/>") ;
			body.append("   <b>Vers�o:</b> "
					+ message.getMessage().getCurrentDictionary().getVersion() + "<br/>");
			body.append("   <b>N�vel de criticidade:</b> " + getEvent().getEventType() + "<br/>");

			body.append("<br/>");
			body.append("   <b>Notifica��o:</b> <br/>");

			Collection<DataField> dataFields = message.getFields();
			if (dataFields != null) {
				for (DataField dataField : dataFields) {
					body.append("    <b>" + dataField.getId()
							+ " (" + ((dataField.getDescription() != null) ? dataField.getDescription() : "")  + ")</b> = '"
									+ FieldObjectUtil.getString(dataField.getField(), dataField.getValue()) + "'  <br/>");
				}
			}

			body.append("</body>");
			body.append("</html>");

			msg.setContent(body.toString(), "text/html; charset=\"iso-8859-1\"");
			Transport.send(msg);*/

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    companion object {

        var PARAM_PROTOCOL = "protocol"
        var PARAM_HOST = "host"
        var PARAM_USER = "user"
        var PARAM_PASSWORD = "password"
        var PARAM_TITLE = "title"
        var PARAM_AUTHENTICATION = "authentication"
        var PARAM_MESSAGE = "message"
    }

}
