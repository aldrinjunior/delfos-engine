package com.delfos.engine.action

import com.delfos.domain.Event
import com.delfos.domain.Rule
import com.delfos.engine.message.MessageModel

class ExecuteSysout(occurrence: String,
                    event: Event,
                    rule: Rule,
                    messageModel: MessageModel,
                    parameters: Map<String, Any>
) : ExecuteAction(occurrence, event, rule, messageModel, parameters) {

    public override fun execute() {
        println(String.format("occurrence: %s, event: %s, rule: %s",
                ocurrence, event.name, rule.name))
    }


}
