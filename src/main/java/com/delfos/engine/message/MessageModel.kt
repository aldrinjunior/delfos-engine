package com.delfos.engine.message

import com.delfos.domain.Component
import com.delfos.domain.Message
import java.util.*

data class MessageModel(val date: Date,
                        val message: Message,
                        val dataFields: Map<String, DataField>,
                        val component: Component, val identifier: String,
                        val raw: String, val hash: String) {
    private var attributes: MutableMap<String, String>? = null

    val name: String?
        get() = message.name

    val description: String?
        get() = message.description

    val fields: Collection<DataField>
        get() = dataFields.values

    val isRegistryLog: Boolean
        get() = message.registryLog

    fun getField(key: String): DataField? {
        return dataFields[key]
    }

    fun addAttribute(key: String, value: String) {
        if (attributes == null) {
            attributes = HashMap()
        }
        attributes!![key] = value
    }

    fun getAttribute(key: String): String? {
        return if (attributes != null) {
            attributes!![key]
        } else {
            null
        }
    }

}
