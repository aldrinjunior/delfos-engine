package com.delfos.engine.rules

import com.delfos.domain.Rule
import com.delfos.engine.exception.ValidationRuleException
import com.delfos.engine.message.MessageModel
import com.delfos.repository.RuleRepository
import javassist.*
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import javax.annotation.PostConstruct


@Service
class ValidationRuleManager @Autowired constructor(private val ruleRepository: RuleRepository?) {

    private val logger = LogFactory.getLog(ValidationRuleManager::class.java)

    private var validationRules: MutableMap<String, ValidationRule>? = null

    private var validationRuleChangeListeners: MutableList<ValidationRuleChangeListener>? = null

    @PostConstruct
    fun init(): Boolean {

        validationRules = ConcurrentHashMap()
        validationRuleChangeListeners = CopyOnWriteArrayList()

        if (ruleRepository != null) {
            val rules = ruleRepository.findAllEnabled()

            val classPool = ClassPool.getDefault()
            classPool.insertClassPath(ClassClassPath(this.javaClass))

            if (rules.isNotEmpty()) {
                for (rule in rules) {
                    try {
                        addRule(rule)
                    } catch (e: ValidationRuleException) {
                        logger.info("Validation rule manager not initialized")
                        return false
                    }

                }
            }
        }

        logger.info("Validation rule manager initialized")
        return true
    }

    fun getValidateRules(): Collection<ValidationRule> {
        return validationRules?.let { it.values } ?: emptyList()
    }

    fun getValidateRule(rule: Rule): ValidationRule? {
        return if (validationRules != null) {
            validationRules!![rule.id]
        } else null
    }

    @Throws(ValidationRuleException::class)
    private fun createValidationRule(rule: Rule): ValidationRule? {
        //ValidationRule validationRule;
        /*if (validateRules.containsKey(rule)) {
                validationRule = validateRules.get(rule);
                className = validationRule.getClass().getName();
                String seq = className.substring(className.length() - 3);
                className = className.substring(0, className.length() - 3);
                className += String.format("%03d", (Integer.parseInt(seq) + 1));
                //className += StringUtil.complete((Integer.parseInt(seq)+1)+"",3,'0',StringUtil.LEFT);
            } else {
                className += "001";
            }*/

        /*if (!validateRules.containsKey(rule.getId())) {
                UUID uuid = UUID.randomUUID();
                className += uuid.toString();
            }*/
        val validationBody = MessageModelValidationRuleBody(rule)

        val body = validationBody.getMethodBody()
        val hash = DigestUtils.sha1Hex(body)

        if (validationRules!!.containsKey(rule.id)) {
            val validationRule = validationRules!![rule.id]

            if (validationRule?.hash.equals(hash, ignoreCase = true)) {
                logger.warn("finded rule with body hash")
                return validationRule
            }
        }

        val validationRule = make(validationBody.getMessageName(), rule, body, hash)
        logger.info(String.format("rule '%s' initialized", rule.name))

        return validationRule

    }

    @Throws(ValidationRuleException::class)
    fun addRules(vararg rules: Rule) {
        val validationRules = ArrayList<ValidationRule>()
        try {
            for (rule in rules) {

                logger.info("adding validator from rule: '" + rule.name + "'")

                val validationRule = createValidationRule(rule)
                if (validationRule != null) {
                    validationRules.add(validationRule)
                }
            }
            cacheValidationRule(*validationRules.toTypedArray())
        } catch (e: ValidationRuleException) {
            logger.error(e.message, e)
            throw e
        }

    }

    @Throws(ValidationRuleException::class)
    fun addRule(rule: Rule) {

        try {

            logger.info("adding validator from rule: '" + rule.name + "'")

            val validationRule = createValidationRule(rule)
            if (validationRule != null) {
                cacheValidationRule(validationRule)
            }

        } catch (e: ValidationRuleException) {
            logger.error(e.message, e)
            throw e
        }

    }

    private fun cacheValidationRule(vararg validationRules: ValidationRule) {

        for (validationRule in validationRules) {

            val rule = validationRule.rule
            this.validationRules!![rule.id!!] = validationRule

            logger.info(String.format("cached rule => '%s'", validationRule.javaClass.name))

            fireValidationRuleChangeListener(rule, null, ValidationRuleEvent.NEW)
        }

    }

    fun removeRule(rule: Rule) {
        if (validationRules != null) {
            if (validationRules!!.containsKey(rule.id)) {

                validationRules!!.remove(rule.id)
                logger.info("validator from rule: " + rule.name + " removed")

                fireValidationRuleChangeListener(rule, null, ValidationRuleEvent.REMOVED)
                //implementar o destroy no ClassPoll
            }
        }
    }

    @Throws(ValidationRuleException::class)
    private fun make(messageName: String, rule: Rule, body: String, hash: String): ValidationRule {

        var className = "RuleId_" + rule.id
        className = className.replace("-".toRegex(), "_")

        val validationRule: ValidationRule
        try {

            val fullClassName = (ValidationRule::class.java.getPackage().name
                    + "." + className + "_" + Date().time)

            val classPool = ClassPool.getDefault()
            classPool.importPackage("com.delfos.engine.rules.ExpressionUtil")
            classPool.insertClassPath(ClassClassPath(this.javaClass))

            var ctClass: CtClass
            try {
                ctClass = classPool.get(fullClassName)
                if (ctClass.isFrozen) {
                    ctClass.detach()
                }
            } catch (e: NotFoundException) {
                logger.debug("$fullClassName not found")
            }

            ctClass = classPool.makeClass(fullClassName)

            ctClass.superclass = classPool.get(ValidationRule::class.java.name)
            val parameters = arrayOf(classPool.get(MessageModel::class.java.name))

            val ctMethod = CtNewMethod.make(CtPrimitiveType.booleanType, "isValid",
                    parameters, null, body, ctClass)
            ctClass.addMethod(ctMethod)

            val clazz = ctClass.toClass() as Class<ValidationRule>
            val constructor = clazz.getDeclaredConstructor(String::class.java, Rule::class.java, String::class.java)
            validationRule = constructor.newInstance(messageName, rule, hash)

        } catch (e: Exception) {
            throw ValidationRuleException("rule " + rule.name
                    + " is not valid", e)
        }

        return validationRule

    }

    private fun fireValidationRuleChangeListener(rule: Rule, ruleOld: Rule?, state: Int) {
        val validationRuleEvent = ValidationRuleEvent(rule, ruleOld, state)
        for (changeListener in validationRuleChangeListeners!!) {
            changeListener.propertyChange(validationRuleEvent)
        }
    }

    fun addValidationRuleChangeListener(changeListener: ValidationRuleChangeListener) {
        validationRuleChangeListeners!!.add(changeListener)
    }

    fun removeValidationRuleChangeListener(changeListener: ValidationRuleChangeListener) {
        validationRuleChangeListeners!!.remove(changeListener)
    }


}
