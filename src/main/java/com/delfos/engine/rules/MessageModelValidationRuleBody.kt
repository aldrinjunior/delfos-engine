package com.delfos.engine.rules

import com.delfos.domain.*
import com.delfos.engine.message.MessageModel


class MessageModelValidationRuleBody(private var rule: Rule) : ValidationRuleBody {

    private var messageName: String? = null

    override fun getMessageName(): String {
        return messageName.orEmpty()
    }

    override fun getRule(): Rule {
        return rule
    }

    override fun getClassType(): Class<*> {
        return MessageModel::class.java
    }

    override fun getMethodBody(): String {
        messageName = rule.conditions?.find { it.type() == ConditionType.MESSAGE }?.value

        val bodyBuilder = MethodBodyBuilder(getClassType(), rule)
        bodyBuilder.add("message", MessageCondition::class.java, Message::class.java)
        bodyBuilder.add("dataFields", MessageDataFieldCondition::class.java, Map::class.java)

        return bodyBuilder.build()
    }

}
