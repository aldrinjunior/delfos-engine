package com.delfos.engine.rules

import com.delfos.domain.Condition
import com.delfos.domain.ConditionType
import com.delfos.domain.Rule
import org.apache.commons.logging.LogFactory

class MethodBodyBuilder(private val classType: Class<*>, private val rule: Rule) {

    private val logger = LogFactory.getLog(MethodBodyBuilder::class.java)

    private val converters = HashMap<Class<*>, Converter>()
    private val expressionBuilder = ExpressionBuilder()

    fun add(alias: String, converter: Class<*>, classType: Class<*>) {
        converters[converter] = Converter(converter, alias, classType)
        logger.trace("bind alias " + alias + " on type " + converter.name)
    }

    private fun getGETMethod(attribute: String): String {
        var method = ""
        var methods = attribute.split(".".toRegex())
                .dropLastWhile { it.isEmpty() }
                .toTypedArray()

        if (methods.isEmpty()) {
            methods = arrayOf(attribute)
        }

        for (i in methods.indices) {
            if (i > 1) {
                method += "."
            }
            method = ("get" + methods[i].substring(0, 1).toUpperCase()
                    + methods[i].substring(1) + "()")
        }
        return method
    }

    private fun getConverters(): Collection<Converter> {
        return converters.values
    }

    fun build(): String {

        val objectName = "event"
        var buffer = " { " + ("	" + classType.name + " " + objectName + " = $1;")

        val collections = getConverters()
        for (converter in collections) {

            buffer += (" " + converter.classType.name)
            /*Class<?>[] parameters = converter.getClassTypeParameters();
            if (parameters != null) {
                if (parameters.length == 1) {
                    buffer = buffer.concat("<" + parameters[0].getName() + ">");
                } else if (parameters.length == 2) {
                    buffer = buffer.concat("<" + parameters[0].getName()
                            + ", " + parameters[1].getName() + ">");
                }
            }*/

            buffer += (" " + converter.alias
                    + " = " + objectName + "." + getGETMethod(converter.alias!!) + ";")

        }

        val conditions = ArrayList(rule.conditions)
        val list = ArrayList<Condition>()
        for (condition in conditions) {
            if (condition.parent != null) {
                list.add(condition)
            }
        }
        conditions.removeAll(list)

        buffer = buffer + " if ( " + build(conditions, StringBuilder()) + " ) { " + " return true; " + "	} else { return false; }" + "}"

        logger.trace("Validation method: $buffer")
        return buffer

    }

    private fun build(conditions: List<Condition>?, buffer: StringBuilder): String {

        if (conditions != null) {

            conditions.sortedWith(Comparator.comparingInt { condition -> condition.order!! })

            for (ruleCondition in conditions) {

                ruleCondition.andOr?.let {
                    buffer.append(expressionBuilder.getOperatorExpression(it))
                }

                if (ruleCondition.type() == ConditionType.GROUP) {
                    buffer.append("(")
                    build(ruleCondition.conditions, buffer)
                    buffer.append(")")
                } else {
                    buffer.append(build(ruleCondition))
                }

            }
        }

        return buffer.toString()
    }

    private fun build(ruleCondition: Condition): String? {
        val interConverter = converters[ruleCondition.javaClass]
        return expressionBuilder.build(interConverter?.alias!!, ruleCondition)
    }

    internal class Converter(var converterType: Class<*>?, var alias: String?, val classType: Class<*>)

}
