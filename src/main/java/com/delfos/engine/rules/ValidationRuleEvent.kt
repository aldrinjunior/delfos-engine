package com.delfos.engine.rules

import com.delfos.domain.Rule

class ValidationRuleEvent constructor(val rule: Rule? = null, val ruleOld: Rule? = null, val state: Int?) {

    constructor(rule: Rule): this(rule, null, NEW)

    constructor(rule: Rule, ruleOld: Rule): this(rule, ruleOld, UPDATED)

    companion object {
        const val NEW = 0
        const val UPDATED = 1
        const val REMOVED = 2
    }
}
