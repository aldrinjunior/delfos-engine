package com.delfos.engine.rules

interface ValidationRuleChangeListener {

    fun propertyChange(validationRuleEvent: ValidationRuleEvent)

}
