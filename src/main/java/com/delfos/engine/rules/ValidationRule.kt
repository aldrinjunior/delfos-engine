package com.delfos.engine.rules

import com.delfos.domain.FieldType
import com.delfos.domain.Message
import com.delfos.domain.Rule
import com.delfos.engine.message.DataField
import com.delfos.engine.message.MessageModel

abstract class ValidationRule(val messageName: String, val rule: Rule, val hash: String) {

    abstract fun isValid(messageModel: MessageModel): Boolean

    fun eq(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        val dataField = dataFields[field] ?: return false
        return when (dataField.type!!) {
            FieldType.STRING -> toEQ(dataFields, field, value)
            FieldType.NUMBER -> {
                if (dataField.isDecimalPoint) {
                    toEQ(dataFields, field, value.toDouble())
                } else {
                    toEQ(dataFields, field, value.toLong())
                }

            }
            FieldType.BOOLEAN -> toEQ(dataFields, field, value.toBoolean())
            FieldType.DATE -> false
        }
    }

    fun ne(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        val dataField = dataFields[field] ?: return false
        return when (dataField.type!!) {
            FieldType.STRING -> toNE(dataFields, field, value)
            FieldType.NUMBER -> {
                if (dataField.isDecimalPoint) {
                    toNE(dataFields, field, value.toDouble())
                } else {
                    toNE(dataFields, field, value.toLong())
                }

            }
            FieldType.BOOLEAN -> toNE(dataFields, field, value.toBoolean())
            FieldType.DATE -> false
        }
    }

    fun gt(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        val dataField = dataFields[field] ?: return false
        return when (dataField.type!!) {
            FieldType.STRING -> toGT(dataFields, field, value)
            FieldType.NUMBER -> {
                if (dataField.isDecimalPoint) {
                    toGT(dataFields, field, value.toDouble())
                } else {
                    toGT(dataFields, field, value.toLong())
                }

            }
            FieldType.BOOLEAN -> false
            FieldType.DATE -> false
        }
    }

    fun lt(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        val dataField = dataFields[field] ?: return false
        return when (dataField.type!!) {
            FieldType.STRING -> toLT(dataFields, field, value)
            FieldType.NUMBER -> {
                if (dataField.isDecimalPoint) {
                    toLT(dataFields, field, value.toDouble())
                } else {
                    toLT(dataFields, field, value.toLong())
                }

            }
            FieldType.BOOLEAN -> false
            FieldType.DATE -> false
        }
    }

    fun ge(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        val dataField = dataFields[field] ?: return false
        return when (dataField.type!!) {
            FieldType.STRING -> toGE(dataFields, field, value)
            FieldType.NUMBER -> {
                if (dataField.isDecimalPoint) {
                    toGE(dataFields, field, value.toDouble())
                } else {
                    toGE(dataFields, field, value.toLong())
                }

            }
            FieldType.BOOLEAN -> false
            FieldType.DATE -> false
        }
    }

    fun le(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        val dataField = dataFields[field] ?: return false
        return when (dataField.type!!) {
            FieldType.STRING -> toLE(dataFields, field, value)
            FieldType.NUMBER -> {
                if (dataField.isDecimalPoint) {
                    toLE(dataFields, field, value.toDouble())
                } else {
                    toLE(dataFields, field, value.toLong())
                }

            }
            FieldType.BOOLEAN -> false
            FieldType.DATE -> false
        }
    }

    fun eq(message: Message, value: String): Boolean {
        return message.name.equals(value, ignoreCase = true)
    }

    fun ne(message: Message, value: String): Boolean {
        return notEquals(message, value)
    }

    fun equals(message: Message, value: String): Boolean {
        return message.name.equals(value, ignoreCase = true)
    }

    fun notEquals(message: Message, value: String): Boolean {
        return !message.name.equals(value, ignoreCase = true)
    }

    private fun toEQ(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        return dataFields[field]?.value.equals(value, ignoreCase = true)
    }

    private fun toNE(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        return !dataFields[field]?.value.equals(value, ignoreCase = true)
    }

    private fun toGT(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        return dataFields[field]?.value!! > value
    }

    private fun toLT(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        return dataFields[field]?.value!! < value
    }

    private fun toGE(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        return dataFields[field]?.value!! >=value
    }

    private fun toLE(dataFields: Map<String, DataField>, field: String, value: String): Boolean {
        return dataFields[field]?.value!! <= value
    }

    private fun toEQ(dataFields: Map<String, DataField>, field: String, value: Boolean): Boolean {
        return dataFields[field]?.value?.toBoolean() == value
    }

    private fun toNE(dataFields: Map<String, DataField>, field: String, value: Boolean): Boolean {
        return dataFields[field]?.value?.toBoolean() != value
    }

    private fun toEQ(dataFields: Map<String, DataField>, field: String, value: Long): Boolean {
        return dataFields[field]?.value?.toLong() == value
    }

    private fun toNE(dataFields: Map<String, DataField>, field: String, value: Long): Boolean {
        return dataFields[field]?.value?.toLong() != value
    }

    private fun toGT(dataFields: Map<String, DataField>, field: String, value: Long): Boolean {
        return dataFields[field]?.value!!.toLong() > value
    }

    private fun toLT(dataFields: Map<String, DataField>, field: String, value: Long): Boolean {
        return dataFields[field]?.value!!.toLong() < value
    }

    private fun toGE(dataFields: Map<String, DataField>, field: String, value: Long): Boolean {
        return dataFields[field]?.value!!.toLong() >=value
    }

    private fun toLE(dataFields: Map<String, DataField>, field: String, value: Long): Boolean {
        return dataFields[field]?.value!!.toLong() <= value
    }

    private fun toEQ(dataFields: Map<String, DataField>, field: String, value: Double): Boolean {
        return dataFields[field]?.value?.toDouble() == value
    }

    private fun toNE(dataFields: Map<String, DataField>, field: String, value: Double): Boolean {
        return dataFields[field]?.value?.toDouble() != value
    }

    private fun toGT(dataFields: Map<String, DataField>, field: String, value: Double): Boolean {
        return dataFields[field]?.value!!.toDouble() > value
    }

    private fun toLT(dataFields: Map<String, DataField>, field: String, value: Double): Boolean {
        return dataFields[field]?.value!!.toDouble() < value
    }

    private fun toGE(dataFields: Map<String, DataField>, field: String, value: Double): Boolean {
        return dataFields[field]?.value!!.toDouble() >=value
    }

    private fun toLE(dataFields: Map<String, DataField>, field: String, value: Double): Boolean {
        return dataFields[field]?.value!!.toDouble() <= value
    }

}
