package com.delfos.engine.rules

import com.delfos.domain.Rule

interface ValidationRuleBody {

    fun getMessageName(): String

    fun getRule(): Rule

    fun getClassType(): Class<*>

    fun getMethodBody(): String

}
