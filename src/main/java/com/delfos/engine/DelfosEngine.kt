package com.delfos.engine

import com.delfos.domain.Component
import com.delfos.domain.DataFieldLog
import com.delfos.domain.Event
import com.delfos.domain.MessageLog
import com.delfos.engine.message.DataField
import com.delfos.engine.message.MessageListener
import com.delfos.engine.message.MessageModel
import com.delfos.engine.message.MessagePayload
import com.delfos.engine.rules.ValidationRule
import com.delfos.engine.rules.ValidationRuleChangeListener
import com.delfos.engine.rules.ValidationRuleEvent
import com.delfos.engine.rules.ValidationRuleManager
import com.delfos.engine.util.FieldObjectUtil
import com.delfos.service.EventService
import com.delfos.service.MessageLogService
import com.delfos.service.MessageService
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.logging.LogFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.Serializable
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Service
class DelfosEngine @Autowired
constructor(private val validationRuleManager: ValidationRuleManager?,
            override val eventManager: EventManager,
            private val messageService: MessageService,
            private val messageLogService: MessageLogService,
            private val eventService: EventService
) : ApplicationEngine, ValidationRuleChangeListener, Serializable {

    private val logger = LogFactory.getLog(ApplicationEngine::class.java)
    private var messageListeners: MutableList<MessageListener>? = null

    override var isInitialized: Boolean = false
    override var date: Date? = null

    @PostConstruct
    override fun init() {

        logger.info("initializing delfos engine")

        messageListeners = CopyOnWriteArrayList()
        try {

            if (validationRuleManager != null) {
                if (!validationRuleManager.init()) {
                    return
                }
            }

            isInitialized = true
            date = Date()

            logger.info("all managers monitoring have been initialized")
            logger.info("delfos engine initialized")

        } catch (e: Exception) {
            logger.error("initializing delfos engine failed", e)
        }

    }

    @PreDestroy
    override fun finish() {
        isInitialized = false
        logger.info("all managers monitoring have been finalized")
    }


    override fun analyze(messagePayload: MessagePayload, component: Component) {

        val now = Date()
        val message = messageService.findByName(messagePayload.name!!)

        val registryLog = message.registryLog
        var fieldsLog: MutableMap<String, DataFieldLog>? = null

        val jsonNode = messagePayload.content

        val dataFields = ConcurrentHashMap<String, DataField>()

        for (field in message.fields!!) {
            if (jsonNode!!.has(field.name)) {

                val fieldNode = jsonNode.get(field.name)
                val dataField = DataField(field, fieldNode.asText(),
                        FieldObjectUtil.getObject(field, fieldNode)!!)

                dataFields[field.name!!] = dataField

                if (registryLog) {
                    if (fieldsLog == null) {
                        fieldsLog = HashMap()
                    }

                    val dataFiledLog = DataFieldLog()
                    dataFiledLog.fieldId = field.id
                    dataFiledLog.name = field.name
                    dataFiledLog.type = field.type
                    dataFiledLog.value = dataField.valueObject
                    fieldsLog[field.name!!] = dataFiledLog
                }

            }
        }

        val json = jsonNode!!.toString()
        val hash = DigestUtils.sha1Hex(json)

        val messageModel = MessageModel(now, message, dataFields,
                component, messagePayload.identifier!!,
                json, hash)

        if (registryLog) {
            val messageLog = MessageLog(null, messagePayload.identifier!!, hash)
            messageLog.dateHour = Date()
            messageLog.messageId = message.id
            messageLog.message = message.name
            messageLog.componentId = component.id
            messageLog.component = Component(component.id!!, component.name)

            if (fieldsLog != null) {
                messageLog.dataFields = ArrayList(fieldsLog.values)
            }
            messageLogService.save(messageLog)
        }

        var validateRules = validationRuleManager!!.getValidateRules()

        validateRules = validateRules
                .filter { validationRule -> validationRule.messageName.equals(messageModel.name!!, ignoreCase = true) }
                .filter { validationRule -> validationRule.isValid(messageModel) }

        if (validateRules.isEmpty()) {
            logger.trace(String.format("rule not found to message '%s'", messageModel.name))
            return
        }

        validateRules.forEach { validationRule ->

            val rule = validationRule.rule
            logger.trace(String.format("rule '%s' is valid to message '%s' ", rule.name, messageModel.name))

            val events = eventService.findByEnabledAndRule(rule)
            if (!events.isNullOrEmpty()) {
                val newEvents = ArrayList<Event>()
                for (alert in events) {
                    if (alert.allRules) {
                        for (ruleChecked in alert.rules!!) {
                            val validateRule = validationRuleManager.getValidateRule(ruleChecked)
                            if (validateRule == null || !validateRule.isValid(messageModel)) {
                                break
                            }
                            newEvents.add(alert)
                        }
                    } else {
                        newEvents.add(alert)
                    }
                }
                logger.trace(String.format("%d events found", newEvents.size))

                eventManager.execute(newEvents, rule, messageModel)
            } else {
                logger.trace(String.format("event not found  to rule '%s'", rule.name))
            }
        }
        fireMessage(messageModel)
    }

    private fun validateRuleLog(message: MessageModel, validateRules: Collection<ValidationRule>?): Boolean {
        if (validateRules == null) {
            return false
        }
        for (validateRule in validateRules) {
            if (validateRule.isValid(message)) {
                return true
            }
        }
        return false
    }

    private fun fireMessage(messageModel: MessageModel) {
        for (listener in this.messageListeners!!) {
            listener.notificationPerformed(messageModel)
        }
    }

    override fun addMessageListener(messageListener: MessageListener) {
        if (this.messageListeners!!.contains(messageListener)) {
            return
        }
        this.messageListeners!!.add(messageListener)
    }

    override fun removeMessageListener(messageListener: MessageListener) {
        this.messageListeners!!.remove(messageListener)
    }

    override fun propertyChange(validationRuleEvent: ValidationRuleEvent) {

    }

}
