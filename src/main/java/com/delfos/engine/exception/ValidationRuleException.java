package com.delfos.engine.exception;

public class ValidationRuleException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ValidationRuleException() {
	}

	public ValidationRuleException(String message) {
		super(message);
	}

	public ValidationRuleException(Throwable cause) {
		super(cause);
	}

	public ValidationRuleException(String message, Throwable cause) {
		super(message, cause);
	}

}
