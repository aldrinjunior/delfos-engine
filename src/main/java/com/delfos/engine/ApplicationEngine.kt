package com.delfos.engine


import com.delfos.domain.Component
import com.delfos.engine.exception.InvalidMonitorException
import com.delfos.engine.message.MessageListener
import com.delfos.engine.message.MessagePayload

import java.util.Date

interface ApplicationEngine {

    val eventManager: EventManager

    val isInitialized: Boolean

    val date: Date?

    fun init()
    fun finish()

    @Throws(InvalidMonitorException::class)
    fun analyze(messagePayload: MessagePayload, component: Component)

    fun addMessageListener(messageListener: MessageListener)

    fun removeMessageListener(messageListener: MessageListener)


}
