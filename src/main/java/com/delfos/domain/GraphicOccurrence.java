package com.delfos.domain;

import java.util.Date;

public class GraphicOccurrence {

    private Date date;
    private long qtdOccurrences;
    private int type;

    public GraphicOccurrence() {

    }

    public GraphicOccurrence(Date date, long qtdOccurrences, int type) {
        super();
        this.date = date;
        this.qtdOccurrences = qtdOccurrences;
        this.setType(type);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getQtdOccurrences() {
        return qtdOccurrences;
    }

    public void setQtdOccurrences(long qtdOccurrences) {
        this.qtdOccurrences = qtdOccurrences;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + type;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GraphicOccurrence other = (GraphicOccurrence) obj;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (type != other.type)
            return false;
        return true;
    }


}
