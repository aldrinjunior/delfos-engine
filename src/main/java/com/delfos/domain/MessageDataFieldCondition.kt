package com.delfos.domain

import com.fasterxml.jackson.annotation.JsonCreator
import javax.persistence.*

@Entity
@DiscriminatorValue(value = "2")
class MessageDataFieldCondition(id: String?) : Condition(id) {

    @JsonCreator(mode = JsonCreator.Mode.DEFAULT)
    constructor(): this(null)

    @ManyToOne
    @JoinColumn(name = "field_id", referencedColumnName = "id")
    var field: Field? = null

    @Column(name = "field_id", insertable = false, updatable = false)
    val fieldId: String? = null
        get() = if (this.field != null && this.field!!.id != null) this.field!!.id else field

    override fun type(): ConditionType? {
        return ConditionType.FIELD
    }

    override fun supportedOperators(): List<String>? {
        val operators = mutableListOf<String>()
        if (field != null) {
            when(field?.type) {
                FieldType.NUMBER -> {
                    operators.add(OperatorType.EQ.value)
                    operators.add(OperatorType.NE.value)
                    operators.add(OperatorType.GTE.value)
                    operators.add(OperatorType.GT.value)
                    operators.add(OperatorType.LTE.value)
                    operators.add(OperatorType.LT.value)
                }
                FieldType.STRING -> {
                    operators.add(OperatorType.EQ.value)
                    operators.add(OperatorType.NE.value)
                }
                FieldType.BOOLEAN -> {
                    operators.add(OperatorType.EQ.value)
                    operators.add(OperatorType.NE.value)
                }
                FieldType.DATE -> {}
            }
            operators.add(OperatorType.IS_NULL.value)
            operators.add(OperatorType.IS_NOT_NUL.value)
        }
        return operators
    }

}
