package com.delfos.domain.converter

import com.delfos.domain.DataFieldLog
import org.springframework.core.convert.converter.Converter
import org.springframework.data.convert.ReadingConverter
import org.springframework.data.mongodb.core.mapping.Document

@ReadingConverter
class DataFieldLogReadConverter : Converter<Document, DataFieldLog> {

    override fun convert(document: Document): DataFieldLog? {
        println(document)
        return null
    }
}
