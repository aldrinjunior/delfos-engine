package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import org.hibernate.annotations.GenericGenerator

import javax.persistence.*


@Entity
@Table(name = "contact_group")
data class ContactGroup(
        @Id
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        @Column(name = "id")
        private var id: String? = null,

        @Column(name = "name", length = 150, nullable = false)
        var name: String? = null,

        @Column(name = "enabled")
        var isEnabled: Boolean = false,

        @ManyToMany(mappedBy = "contactGroups")
        @OrderBy("name")
        var contacts: MutableList<Contact>? = null
) : Identifier<String> {

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ContactGroup) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
 
