package com.delfos.domain

import com.eoscode.springapitools.data.domain.Find
import com.eoscode.springapitools.data.domain.Identifier
import com.eoscode.springapitools.data.domain.NoDelete
import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table(name = "component")
@DynamicUpdate
@NoDelete
@Find
data class Component(
        @Id
        @Column(name = "id")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        private var id: String? = null,

        @NotNull
        @Size(min = 2, max = 150)
        @Column(name = "name", nullable = false, length = 150)
        var name: String?,

        @Column(name = "description", length = 500)
        var description: String? = null,

        @JsonIgnore
        @Column(name = "token", length = 1024)
        var token: String? = null,

        @NotNull
        @Column(name = "enabled", length = 1)
        var enabled: Boolean? = false

) : Auditable(), Identifier<String> {

    @Column(name = "status", length = 1)
    @ColumnDefault("1")
    val status: Int? = null

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Component) return false
        val component = other as Component?
        return id == component!!.id
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }

}
