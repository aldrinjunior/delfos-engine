package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import org.hibernate.annotations.GenericGenerator
import org.hibernate.validator.constraints.Length
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "action_type")
@NamedEntityGraphs(NamedEntityGraph(name = "ActionType.findDetailById",
        attributeNodes = [NamedAttributeNode("parameters")]))
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
data class ActionType(
        @Id
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        @Column(name = "id")
        private var id: String? = null,

        @NotNull
        @Length(min = 2, max = 150)
        @Column(name = "name", length = 150, nullable = false)
        var name: String? = null,

        @Column(name = "description", length = 300)
        var description: String? = null,

        @Column(name = "executable", length = 500)
        var executable: String? = null,

        @Column(name = "system_action", nullable = false)
        var isSystem: Boolean = false,

        @JsonIgnoreProperties("actionType")
        @OneToMany(mappedBy = "actionType", cascade = [CascadeType.PERSIST,
            CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.MERGE],
                targetEntity = ActionTypeParameter::class)
        @OrderBy(value = "name")
        var parameters: MutableList<ActionTypeParameter>? = null,

        @Column(name = "path", length = 500)
        var path: String? = null
) : Identifier<String> {

    constructor(id: String) : this() {
        this.id = id
    }

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    fun addParameter(parameter: ActionTypeParameter) {
        if (parameters == null) {
            parameters = ArrayList()
        }
        parameter.actionType = this
        parameters!!.add(parameter)
    }

    fun removeParameter(parameter: ActionTypeParameter) {
        if (parameters != null) {
            parameters!!.remove(parameter)
        }
    }

}
