package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.format.annotation.DateTimeFormat
import java.util.*

@Document(collection = "event_log")
data class EventLog(
        @Id
        private var id: String? = null,

        @Indexed
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        var dateHour: Date? = null,

        @Indexed
        var eventId: String? = null,

        @Indexed
        var eventType: EventType? = null,

        var event: Event? = null,

        @Indexed
        var ruleId: String? = null,

        var rule: Rule? = null,

        @Indexed
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        var delayStart: Date? = null,

        @Indexed
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        var delayEnd: Date? = null,

        @Indexed
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        var lastModifiedDate: Date? = null,

        var count: Long = 0,

        var type: Int = 0,

        var dataFields: MutableList<DataFieldLog>? = null,

        @Indexed
        var messageId: String? = null,

        var message: Message? = null,

        @Indexed
        var identifier: String? = null,

        @Indexed
        var componentId: String? = null,

        var component: Component? = null,

        @Indexed
        var hash: String? = null,

        var eventStartId: String? = null
) : Identifier<String> {

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    fun addDataField(dataFieldLog: DataFieldLog) {
        if (dataFields == null) {
            dataFields = ArrayList()
        }
        dataFields?.add(dataFieldLog)
    }

    fun removeDataField(dataField: DataFieldLog) {
        dataFields?.remove(dataField)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is EventLog) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
