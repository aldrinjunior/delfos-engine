package com.delfos.domain

enum class ExpressionType {
    MESSAGE_NAME,
    MESSAGE_DATA_FIELD
}
