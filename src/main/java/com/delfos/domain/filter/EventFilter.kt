package com.delfos.domain.filter

import com.delfos.domain.EventType
import com.delfos.domain.Rule
import com.delfos.engine.message.DataField

data class EventFilter(
        var eventId: String? = null,
        var rules: List<Rule>? = null,
        var eventType: EventType? = null,
        var messageId: String? = null,
        var message: String? = null,
        var dataFields: List<DataField> = ArrayList()
)
