package com.delfos.domain.filter

data class MessageLogStatisticFilter(
        var filters: MessageLogFilter? = null,
        var groupBy: List<String>? = null,
        var sortBy: List<String>? = null,
        var limit: Long? = null
)
