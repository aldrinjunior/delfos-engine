package com.delfos.domain.filter

import com.delfos.domain.EventType
import java.util.*

data class EventLogFilter(
        var id: String? = null,
        var componentId: String? = null,
        var identifier: String? = null,
        var dateHour: Date? = null,
        var dateHourEnd: Date? = null,
        var eventId: String? = null,
        var ruleId: String? = null,
        var eventType: EventType? = null,
        var messageId: String? = null,
        var message: String? = null,
        var hash: String? = null,
        var dataFields: List<DataFieldLogFilter> = ArrayList()
)
