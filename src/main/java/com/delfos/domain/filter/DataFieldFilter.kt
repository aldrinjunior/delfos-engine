package com.delfos.domain.filter

data class DataFieldFilter(
        var fieldId: String? = null,
        var operator: String? = null,
        var value: Any? = null
)
