package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import org.hibernate.annotations.GenericGenerator
import javax.persistence.*

@Entity
@Table(name = "message_group")
class MessageGroup(
        @Id
        @GeneratedValue(generator = "uuid")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @Column
        private var id: String? = null,

        @Column(nullable = false, length = 100)
        var name: String? = null,

        @OneToMany(mappedBy = "messageGroup", fetch = FetchType.LAZY,
                targetEntity = Message::class, cascade = [CascadeType.ALL])
        var messages: List<Message>? = null
) : Identifier<String> {

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MessageGroup) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
