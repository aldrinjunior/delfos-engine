package com.delfos.domain.view

import com.delfos.domain.Component
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.format.annotation.DateTimeFormat.ISO.DATE
import org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME
import java.util.*

data class MessageLogStatisticView(
        @DateTimeFormat(iso = DATE)
        var date: Date? = null,

        @DateTimeFormat(iso = DATE_TIME)
        var dateHour: Date? = null,

        @JsonIgnoreProperties(value = ["enabled"])
        var component: Component? = null,

        var identifier: String? = null,

        var message: String? = null,

        var count: Long? = null
)
