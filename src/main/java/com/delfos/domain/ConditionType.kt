package com.delfos.domain

enum class ConditionType constructor(val value: Int) {
    GROUP(0),
    MESSAGE(1),
    FIELD(2);
}
