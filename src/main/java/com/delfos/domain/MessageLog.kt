package com.delfos.domain

import com.fasterxml.jackson.annotation.JsonAutoDetect
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.format.annotation.DateTimeFormat
import java.io.Serializable
import java.util.*

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, setterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@Document(collection = "message_log")
data class MessageLog(
        @Id
        var id: String? = null,

        @Indexed
        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
        var dateHour: Date? = null,

        @Indexed
        var messageId: String? = null,

        @Indexed
        var message: String? = null,

        var dataFields: List<DataFieldLog> = ArrayList(),

        @Indexed
        var identifier: String? = null,

        @Indexed
        var componentId: String? = null,

        var component: Component? = null,

        @Indexed
        var hash: String? = null
) : Serializable {

    constructor(id: String?, hash: String) : this(id) {
        this.hash = hash
    }

    constructor(id: String?, identifier: String, hash: String) : this(id, hash) {
        this.identifier = identifier
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MessageLog) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

}
