package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import com.fasterxml.jackson.annotation.*
import org.hibernate.annotations.DynamicUpdate
import org.hibernate.annotations.GenericGenerator

import javax.persistence.*
import java.io.Serializable
import java.util.ArrayList
import java.util.Objects

@Entity
@Table(name = "rule_condition")
@DynamicUpdate
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.INTEGER, length = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class, property = "id")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", include = JsonTypeInfo.As.EXISTING_PROPERTY)
@JsonSubTypes(JsonSubTypes.Type(value = GroupCondition::class, name = "0"),
        JsonSubTypes.Type(value = MessageCondition::class, name = "1"),
        JsonSubTypes.Type(value = MessageDataFieldCondition::class, name = "2"))
data class Condition(
        @Id
        @Column(name = "id")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @GeneratedValue(generator = "uuid")
        private var id: String?
) : Identifier<String>, Serializable {

    @JsonCreator(mode = JsonCreator.Mode.DEFAULT)
    constructor() : this(null)

    @Column(name = "operator")
    var operator: String? = null

    @JsonIgnoreProperties(value = ["rule", "parent", "message"])
    @OneToMany(mappedBy = "parent", cascade = [CascadeType.PERSIST,
        CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.MERGE],
            targetEntity = Condition::class, fetch = FetchType.EAGER)
    @OrderBy(value = "order")
    var conditions: MutableList<Condition>? = null

    @Column(name = "parent_id", insertable = false, updatable = false)
    val parentId: String? = null
        get() = if (parent != null && parent!!.id != null) parent!!.id else field

    @ManyToOne(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST])
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    var parent: Condition? = null

    @Column(name = "rule_id", insertable = false, updatable = false)
    val ruleId: String? = null
        get() = if (rule != null && rule!!.id != null) rule!!.id else field

    @ManyToOne
    @JoinColumn(name = "rule_id")
    var rule: Rule? = null

    @Column(name = "condition_order")
    var order: Int? = null

    @Column(name = "expression_type")
    var expressionType: Int = 0

    @Column(name = "value", length = 250)
    var value: String? = null

    @Column(name = "andOr")
    var andOr: String? = null

    @Column(name = "type", insertable = false, updatable = false)
    private val type: Int? = null

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    fun supportedOperators(): List<String>? {
        return null
    }

    fun type(): ConditionType? {
        return null
    }

    fun addCondition(condition: Condition) {
        if (conditions == null) {
            conditions = ArrayList()
        }

        if (condition.parent == null) {
            condition.parent = this
        }

        if (condition.rule == null) {
            condition.rule = this.rule
        }

        if (condition.order == null) {
            val order = if (conditions == null) 0 else conditions!!.size
            condition.order = order
        }

        conditions!!.add(condition)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Condition) return false
        val condition = other as Condition?
        return id == condition!!.id
    }

    override fun hashCode(): Int {
        return Objects.hash(id)
    }

}
