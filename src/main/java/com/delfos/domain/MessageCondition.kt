package com.delfos.domain

import com.fasterxml.jackson.annotation.JsonCreator
import java.util.*
import javax.persistence.DiscriminatorValue
import javax.persistence.Entity


@Entity
@DiscriminatorValue(value = "1")
class MessageCondition(id: String?) : Condition(id) {

    @JsonCreator(mode = JsonCreator.Mode.DEFAULT)
    constructor(): this(null)

    override fun type(): ConditionType? {
        return ConditionType.MESSAGE
    }

    override fun supportedOperators(): List<String>? {
        val operators = ArrayList<String>()

        if (expressionType == CONDITION_NAME) {
            operators.add(OperatorType.EQ.value)
            operators.add(OperatorType.NE.value)
        } else if (expressionType == CONDITION_REGISTRY_LOG) {
            operators.add(OperatorType.EQ.value)
            operators.add(OperatorType.NE.value)
        } else if (expressionType == CONDITION_SYSTEM) {
            operators.add(OperatorType.EQ.value)
            operators.add(OperatorType.NE.value)
        } else if (expressionType == CONDITION_NIVEL) {
            operators.add(OperatorType.EQ.value)
            operators.add(OperatorType.NE.value)
            operators.add(OperatorType.GTE.value)
            operators.add(OperatorType.GT.value)
            operators.add(OperatorType.LTE.value)
            operators.add(OperatorType.LT.value)
        }

        return operators
    }

    companion object {
        const val CONDITION_NAME = 1
        const val CONDITION_SYSTEM = 2
        const val CONDITION_NIVEL = 3
        const val CONDITION_REGISTRY_LOG = 4
    }

}
