package com.delfos.domain

import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener

import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass
import javax.persistence.Temporal
import java.io.Serializable
import java.util.Date

import javax.persistence.TemporalType.TIMESTAMP

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
class Auditable : Serializable {

    @CreatedBy
    var createdBy: String? = null

    @CreatedDate
    @Temporal(TIMESTAMP)
    var creationDate: Date? = null

    @LastModifiedBy
    var lastModifiedBy: String? = null

    @LastModifiedDate
    @Temporal(TIMESTAMP)
    var lastModifiedDate: Date? = null

}
