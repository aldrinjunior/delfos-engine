package com.delfos.domain

import com.eoscode.springapitools.data.domain.Identifier
import org.hibernate.annotations.GenericGenerator

import javax.persistence.*

@Entity
@Table(name = "contact")
data class Contact(
        @Id
        @GeneratedValue(generator = "uuid")
        @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
        @Column(name = "id")
        private var id: String? = null,

        @Column(name = "email", nullable = false, length = 250)
        var email: String? = null,

        @Column(name = "name", nullable = false, length = 100)
        var name: String? = null,

        @Column(name = "phone", length = 20)
        var phone: String? = null,

        @Column(name = "status")
        var isEnabled: Boolean = false,

        @ManyToMany(cascade = [CascadeType.PERSIST], targetEntity = ContactGroup::class)
        @JoinTable(name = "contact_group_association", joinColumns = [JoinColumn(name = "contact_id",
                referencedColumnName = "id")],
                inverseJoinColumns = [JoinColumn(name = "contact_group_id", referencedColumnName = "id")])
        @OrderBy(value = "name")
        var contactGroups: MutableList<ContactGroup>? = null
) : Identifier<String> {

    override fun getId(): String? {
        return id
    }

    override fun setId(id: String?) {
        this.id = id
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Contact) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}
 
