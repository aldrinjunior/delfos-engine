package com.delfos.domain

enum class OperatorType (val value: String, val expression: String?) {

    EQ("eq", "=="),
    NE("ne", "!="),
    LIKE("like", null),
    GT("gt", ">"),
    LT("lt", "<"),
    GTE("gte", ">="),
    LTE("lte", "<="),
    IN("in", null),
    BTW("btw", null),
    IS_NULL("isNull", "== null"),
    IS_NOT_NUL("isNotNull", " != null"),
    OR("or", " || "),
    AND("and", " && "),
    GP("gp", null);

}
