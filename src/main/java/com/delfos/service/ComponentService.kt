package com.delfos.service

import com.delfos.domain.Component
import com.delfos.repository.ComponentRepository
import com.eoscode.springapitools.service.AbstractService
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class ComponentService @Autowired
constructor(private val componentRepository: ComponentRepository) : AbstractService<ComponentRepository, Component, String>() {

    @Value("\${security.jwt.component}")
    private val secret: String? = null

    override fun getRepository(): ComponentRepository {
        return componentRepository
    }

    @Transactional
    override fun save(component: Component): Component {
        val newComponent = component.id == null

        if (newComponent) {
            component.token = UUID.randomUUID().toString()
        } else {
            val token = findById(component.id)?.token
            component.token = token
        }
        return super.save(component)
    }

    @Transactional
    fun resetToken(componentId: String): Component? {
        val component: Component? = findById(componentId)
        component?.apply {
            token = UUID.randomUUID().toString()
        }
        return super.save(component)
    }

    fun generateSignatureToken(token: String): String {
        return Jwts.builder()
                .setIssuedAt(Date())
                .setSubject(token)
                .signWith(SignatureAlgorithm.HS256, secret!!.toByteArray())
                .compact()
    }

    fun isSignatureValid(signature: String): Boolean {
        val claims = getClaims(signature)
        return claims != null
    }

    fun findBySignatureToken(signature: String): Component? {
        val claims = getClaims(signature)
        return if (claims != null) {
            repository.findByToken(claims.subject)
        } else null
    }

    private fun getClaims(token: String): Claims? {
        try {
            return Jwts.parser().setSigningKey(secret!!.toByteArray()).parseClaimsJws(token).body
        } catch (e: Exception) {
            return null
        }
    }

}
