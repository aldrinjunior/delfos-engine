package com.delfos.service

import com.delfos.domain.Action
import com.delfos.domain.ActionType
import com.delfos.domain.ActionTypeParameter
import com.delfos.repository.ActionParameterRepository
import com.delfos.repository.ActionRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ActionService @Autowired
constructor(private val actionRepository: ActionRepository,
            private val actionParameterRepository: ActionParameterRepository) : AbstractService<ActionRepository, Action, String>() {

    override fun getRepository(): ActionRepository {
        return actionRepository
    }

    @Transactional
    override fun save(entity: Action): Action {
        if (entity.id != null) {
            val actionOld = findDetailById(entity.id)

            actionOld?.parameters?.apply {
                if (size > 0) {
                    removeAll(entity.parameters!!)
                    forEach { parameter -> actionParameterRepository.deleteById(parameter.id!!) }
                }
            }
        }

        entity.parameters?.apply {
            forEach { parameter ->
                if (parameter.action == null) {
                    parameter.action = entity
                }
                if (parameter.type == null && parameter.typeId != null) {
                    parameter.type = ActionTypeParameter(parameter.typeId!!)
                }
            }
        }

        if (entity.actionType == null && entity.getActionTypeId() != null) {
            entity.actionType = ActionType(entity.getActionTypeId()!!)
        }

        return super.save(entity)
    }

}
