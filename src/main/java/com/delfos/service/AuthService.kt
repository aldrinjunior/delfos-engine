package com.delfos.service

import com.eoscode.springapitools.security.Auth
import com.eoscode.springapitools.service.exceptions.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException

class AuthService @Autowired
constructor(private val userService: UserService) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {

        try {

            val user = userService.findByUsername(username)

            return Auth(user!!.id, user.username, user.password, null)

        } catch (e: EntityNotFoundException) {
            throw UsernameNotFoundException("Username not found.")
        }

    }

}
