package com.delfos.service

import com.delfos.domain.Event
import com.delfos.domain.EventType
import com.delfos.domain.filter.EventLogFilter
import com.delfos.domain.filter.EventLogStatisticFilter
import com.delfos.domain.view.CountView
import com.delfos.domain.view.EventLogStatisticView
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.function.Supplier
import java.util.stream.Stream
import kotlin.streams.toList

@Service
class StatisticService @Autowired
constructor(private val messageLogService: MessageLogService,
            private val eventLogService: EventLogService) {

    private fun countMessages(): CountView {

        val views = messageLogService.statisticQueryByDate(Date(), null, null)
        return if (views.size == 1) {
            CountView("messages", views[0].count)
        } else CountView("messages", 0L)

    }

    private fun countEvents(): CountView {

        val views = eventLogService.statisticQueryByDate(Date(), null, null)
        return if (views.size == 1) {
            CountView("events", views[0].count)
        } else CountView("events", 0L)

    }

    private fun countEventLogByEventTypeError(): CountView {

        val filterBy = EventLogFilter()
        filterBy.dateHour = Date()
        filterBy.eventType = EventType.ERROR

        val statisticalFilter = EventLogStatisticFilter()
        statisticalFilter.filters = filterBy
        statisticalFilter.groupBy = listOf("eventType")

        val views = eventLogService.statisticQuery(statisticalFilter)
        return if (views.size == 1) {
            CountView("eventTypeError", views[0].count)
        } else CountView("eventTypeError", 0L)

    }

    fun countEventLogByEvent(eventType: EventType?): List<EventLogStatisticView>? {

        val filterBy = EventLogFilter()
        if (eventType != null) {
            filterBy.eventType = eventType
        }
        filterBy.dateHour = Date()

        val statisticalFilter = EventLogStatisticFilter()
        statisticalFilter.filters = filterBy
        statisticalFilter.groupBy = listOf("event")
        statisticalFilter.limit = 5L

        return eventLogService.statisticQuery(statisticalFilter)

    }

    fun countEventLogByEventAndEventTypeError(): List<EventLogStatisticView> {

        val filterBy = EventLogFilter()
        filterBy.eventType = EventType.ERROR
        filterBy.dateHour = Date()

        val statisticalFilter = EventLogStatisticFilter()
        statisticalFilter.filters = filterBy
        statisticalFilter.groupBy = listOf("event")
        statisticalFilter.limit = 5L

        return eventLogService.statisticQuery(statisticalFilter)

    }

    fun countEventLogByEventType(): List<EventLogStatisticView> {

        val filterBy = EventLogFilter()
        filterBy.dateHour = Date()

        val statisticalFilter = EventLogStatisticFilter()
        statisticalFilter.filters = filterBy
        statisticalFilter.groupBy = listOf("eventType")

        return eventLogService.statisticQuery(statisticalFilter)

    }

    fun summary(): List<CountView> {
        val tasks = Stream.of(
                Supplier { this.countMessages() },
                Supplier { this.countEvents() },
                Supplier { this.countEventLogByEventTypeError() })

        return tasks
                .map { CompletableFuture.supplyAsync(it) }
                .map { it.join() }
                .toList()
    }

    fun countEventLogByDateHourAndEvent(): List<EventLogStatisticView>? {

        // recupera os top 5 eventos de hoje
        val list = countEventLogByEvent(null)
        if (list.isNullOrEmpty()) {
            return ArrayList()
        }

        val topEvents: List<Event> = list
                .filter { it.event != null }
                .map { it.event!! }

        val instant = Instant.now()
        val instantHourEarlier = instant.minus(12, ChronoUnit.HOURS)

        return eventLogService.countEventLogByDateHourAndEvent(
                Date.from(instantHourEarlier),
                Date.from(instant),
                topEvents, null)
    }


}
