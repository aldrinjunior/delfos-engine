package com.delfos.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import com.delfos.domain.MessageGroup
import com.delfos.repository.MessageGroupRepository
import com.eoscode.springapitools.service.AbstractService

@Service
class MessageGroupService : AbstractService<MessageGroupRepository, MessageGroup, String>() {

    @Autowired
    private val messageGroupRepository: MessageGroupRepository? = null

    override fun getRepository(): MessageGroupRepository? {
        return messageGroupRepository
    }

}
