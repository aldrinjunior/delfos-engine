package com.delfos.service.validation

import com.delfos.domain.Rule
import com.eoscode.springapitools.resource.exception.FieldMessage

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import java.util.ArrayList

class RuleValidator : ConstraintValidator<RuleSaveUpdate, Rule> {

    override fun isValid(value: Rule, context: ConstraintValidatorContext): Boolean {

        val list = ArrayList<FieldMessage>()

        if (value.conditions == null || value.conditions!!.isEmpty()) {
            list.add(FieldMessage("conditions", "invalid condition for rule"))
        }

        for (e in list) {
            context.disableDefaultConstraintViolation()
            context.buildConstraintViolationWithTemplate(e.message).addPropertyNode(e.fieldName)
                    .addConstraintViolation()
        }

        return list.isEmpty()
    }

    override fun initialize(constraintAnnotation: RuleSaveUpdate?) {

    }

}
