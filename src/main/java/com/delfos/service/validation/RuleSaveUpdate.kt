package com.delfos.service.validation

import javax.validation.Constraint
import javax.validation.Payload
import kotlin.reflect.KClass

@Constraint(validatedBy = [RuleValidator::class])
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class RuleSaveUpdate(val message: String = "Validation error",
                                val groups: Array<KClass<*>> = [],
                                val payload: Array<KClass<out Payload>> = [])
