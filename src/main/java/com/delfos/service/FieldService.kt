package com.delfos.service

import com.delfos.domain.Field
import com.delfos.repository.FieldRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class FieldService : AbstractService<FieldRepository, Field, String>() {
    @Autowired
    private val fieldRepository: FieldRepository? = null

    override fun getRepository(): FieldRepository? {
        return fieldRepository
    }

}
