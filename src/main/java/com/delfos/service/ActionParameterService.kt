package com.delfos.service

import com.delfos.domain.ActionParameter
import com.delfos.repository.ActionParameterRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ActionParameterService : AbstractService<ActionParameterRepository, ActionParameter, String>() {

    @Autowired
    private val actionParameterRepository: ActionParameterRepository? = null

    override fun getRepository(): ActionParameterRepository? {
        return actionParameterRepository
    }

}
