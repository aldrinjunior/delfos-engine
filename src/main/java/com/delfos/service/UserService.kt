package com.delfos.service

import com.delfos.domain.User
import com.delfos.repository.UserRepository
import com.eoscode.springapitools.security.Auth
import com.eoscode.springapitools.security.AuthenticationContext
import com.eoscode.springapitools.service.AbstractService
import com.eoscode.springapitools.service.exceptions.AuthorizationException
import com.eoscode.springapitools.service.exceptions.EntityNotFoundException
import com.eoscode.springapitools.service.exceptions.ValidationException
import com.eoscode.springapitools.util.NullAwareBeanUtilsBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.lang.reflect.InvocationTargetException

@Service
class UserService @Autowired
constructor(private val userRepository: UserRepository,
            private val bCryptPasswordEncoder: BCryptPasswordEncoder
) : AbstractService<UserRepository, User, String>(), UserDetailsService {

    override fun getRepository(): UserRepository {
        return userRepository
    }

    @Transactional
    override fun save(user: User): User {

        try {
            val userNameOld = findByUsername(user.username)
            userNameOld?.let {
                if (user.id == null || !user.id.equals(it.id, ignoreCase = true)) {
                    throw ValidationException("Username já cadastrado.")
                }
            }
        } catch (e: EntityNotFoundException) {}

        var newPassword = true
        if (user.id != null) {
            val userOld = findById(user.id)

            try {
                NullAwareBeanUtilsBean.getInstance().copyProperties(userOld, user)
                if (user.password == null && userOld.password != null) {
                    newPassword = false
                    user.password = userOld.password
                }
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            } catch (e: InvocationTargetException) {
                e.printStackTrace()
            }

        }

        if (newPassword && user.password != null) {
            user.password = bCryptPasswordEncoder.encode(user.password!!)
        }
        return super.save(user)
    }

    @Transactional
    fun changePasswordCurrentUser(password: String, newPassword: String): User {

        if (!AuthenticationContext.authenticated().isPresent) {
            throw AuthorizationException("invalid token.")
        }

        val auth = AuthenticationContext.authenticated().get()
        val user = findById(auth.id as String)

        if (!bCryptPasswordEncoder.matches(password, user.password)) {
            throw ValidationException("Senha atual não confere.")
        }

        user.password = bCryptPasswordEncoder.encode(newPassword)
        return update(user)

    }

    @Transactional
    fun changePassword(id: String, password: String): User {
        val userOld = findById(id)
        userOld.password = bCryptPasswordEncoder.encode(password)

        return update(userOld)
    }

    @Throws(EntityNotFoundException::class)
    fun findByUsername(username: String?): User? {
        return repository.findUserByUsername(username!!).orElseThrow { EntityNotFoundException("User not found. username: $username") }
    }

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        try {
            val user = findByUsername(username)
            return Auth(user!!.id, user.username, user.password, null)
        } catch (e: EntityNotFoundException) {
            throw UsernameNotFoundException("Username not found.")
        }
    }

}
