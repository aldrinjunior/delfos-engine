package com.delfos.service

import com.delfos.domain.Event
import com.delfos.domain.EventLog
import com.delfos.domain.MessageLog
import com.delfos.domain.Rule
import com.delfos.domain.filter.EventLogFilter
import com.delfos.domain.filter.EventLogStatisticFilter
import com.delfos.domain.view.EventLogStatisticView
import com.delfos.repository.mongo.EventLogRepository
import com.delfos.service.mongo.AbstractMongoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort.Direction
import org.springframework.data.domain.Sort.by
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation.*
import org.springframework.data.mongodb.core.aggregation.AggregationOperation
import org.springframework.data.mongodb.core.aggregation.DateOperators
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.repository.support.PageableExecutionUtils
import org.springframework.stereotype.Service
import java.util.*

@Service
class EventLogService @Autowired
constructor(private val mongoTemplate: MongoTemplate,
            override val repository: EventLogRepository) : AbstractMongoService<EventLogRepository, EventLog, String>() {

    fun findByEventAndRule(event: Event, rule: Rule, identifier: String?, hash: String?): Optional<EventLog> {

        val criteria = Criteria()
        val criterias = ArrayList<Criteria>()

        criterias.add(where("eventId").`is`(event.id))
        criterias.add(where("ruleId").`is`(rule.id))
        criterias.add(where("delayStart").exists(true))

        if (identifier != null) {
            criterias.add(where("identifier").`is`(identifier))
        }

        if (hash != null) {
            criterias.add(where("hash").`is`(hash))
        }

        criteria.andOperator(*criterias.toTypedArray())

        val query = Query(criteria)
                .with(by(Direction.DESC, "delayStart"))
                .limit(1)

        query.fields().exclude("dataFields")

        val eventLog = mongoTemplate.findOne(query, EventLog::class.java)
        return Optional.ofNullable(eventLog)
    }

    fun findByLastEventLog(): Optional<List<EventLog>> {

        val criteria = where("startDelay").exists(false)
                .orOperator(where("startDelay").not())

        val query = Query(criteria)
                .with(by(Direction.DESC, "dateHour"))
                .limit(20)

        query.fields()
                .include("_id")
                .include("dateHour")
                .include("eventId")
                .include("ruleId")
                .include("startDelay")
                .include("count")
                .include("hash")

        return Optional.ofNullable(mongoTemplate.find(query, EventLog::class.java))
    }

    fun find(filterBy: EventLogFilter, pageable: Pageable): Page<EventLog> {

        val criterias = buildCriteriaList(filterBy)

        val criteria = Criteria()
        if (criterias != null && criterias.isNotEmpty()) {
            criteria.andOperator(*criterias.toTypedArray())
        }
        val query = Query(criteria).with(pageable).with(by(Direction.DESC,
                "dateHour"))

        query.fields().exclude("dataFields")

        var list: List<EventLog> = ArrayList()
        try {
            list = mongoTemplate.find(query, EventLog::class.java)
        } catch (e: Exception) {
            log.error("find event log:", e)
        }

        return PageableExecutionUtils.getPage(
                list,
                pageable
        ) { mongoTemplate.count(query, MessageLog::class.java) }
    }

    fun statisticQuery(statisticBy: EventLogStatisticFilter): List<EventLogStatisticView> {

        val operations = ArrayList<AggregationOperation>()

        val criterias = buildCriteriaList(statisticBy.filters)
        if (criterias != null && criterias.isNotEmpty()) {
            val criteria = Criteria()
            operations.add(match(criteria.andOperator(*criterias.toTypedArray())))
        }

        if (!statisticBy.groupBy.isNullOrEmpty()) {
            statisticBy.groupBy?.apply {
                val groups = toTypedArray()
                val projections = toTypedArray()

                if (groups.isNotEmpty()) {

                    var projectionOperation = project()

                    for (field in projections) {
                        if (field.equals("date", ignoreCase = true)) {
                            projectionOperation = projectionOperation.and(DateOperators.DateToString.dateOf("dateHour")
                                    .toString("%Y-%m-%d")).`as`("dateWithOutHour")
                        } else if (field.equals("dateHour", ignoreCase = true)) {
                            projectionOperation = projectionOperation.and(DateOperators.DateToString.dateOf("dateHour")
                                    .toString("%Y-%m-%d %H")).`as`("dateWithHour")
                        } else {
                            projectionOperation = projectionOperation.and(field).`as`(field)
                        }
                    }
                    operations.add(projectionOperation)

                    projectionOperation = project()
                    for (field in projections) {
                        if (field.equals("date", ignoreCase = true)) {
                            projectionOperation = projectionOperation.and(DateOperators.DateFromString.fromStringOf("dateWithOutHour")
                                    .withFormat("%Y-%m-%d")).`as`("date")
                        } else if (field.equals("dateHour", ignoreCase = true)) {
                            projectionOperation = projectionOperation.and(DateOperators.DateFromString.fromStringOf("dateWithHour")
                                    .withFormat("%Y-%m-%d %H")).`as`("dateHour")
                        } else {
                            projectionOperation = projectionOperation.and(field).`as`(field)
                        }
                    }
                    operations.add(projectionOperation)

                    operations.add(group(*groups).count().`as`("count"))

                    if (projections.size > 1) {
                        operations.add(project("count").andInclude(*projections))
                    } else if (projections.size == 1) {
                        operations.add(project("count").and(projections[0]).previousOperation())
                    }
                }
            }
        } else {
            operations.add(count().`as`("count"))
        }

        operations.add(sort(Direction.DESC, "count"))
        statisticBy.limit?.let { operations.add(limit(it)) }

        val agg = newAggregation(operations)
        val groupResults = mongoTemplate.aggregate(agg, EventLog::class.java, EventLogStatisticView::class.java)
        return groupResults.mappedResults
    }

    fun statisticQueryByDate(
            dateHour: Date?,
            dateHourEnd: Date?,
            groups: Array<String>?): List<EventLogStatisticView> {

        val aggregateBy = EventLogStatisticFilter()
        groups?.let { aggregateBy.groupBy = it.toList() }

        val filter = EventLogFilter()
        filter.dateHour = dateHour?.let { it } ?: Date()
        filter.dateHourEnd = dateHourEnd

        aggregateBy.filters = filter

        return statisticQuery(aggregateBy)

    }

    fun countEventLogByDateHourAndEvent(dateHour: Date?,
                                        dateHourEnd: Date,
                                        events: List<Event>?,
                                        limit: Long?): List<EventLogStatisticView>? {

        val operations = ArrayList<AggregationOperation>()

        if (dateHour != null) {
            val matchOperation = match(Util.dateToBetweenCriteria("dateHour",
                    dateHour, dateHourEnd))
            operations.add(matchOperation)
        }

        if (!events.isNullOrEmpty()) {
            val eventIds = events.map { it.id }
            operations.add(match(Criteria.where("eventId").`in`(eventIds)))
        }

        val projectionOperation = project()
                .and(DateOperators.DateToString.dateOf("dateHour")
                        .toString("%Y-%m-%d %H")).`as`("dateWithHour")
                .and("event").`as`("event")
        operations.add(projectionOperation)

        val groupOperation = group("dateWithHour", "event")
                .count().`as`("count")
        operations.add(groupOperation)

        operations.add(project("count")
                .and(DateOperators.DateFromString.fromStringOf("dateWithHour")
                        .withFormat("%Y-%m-%d %H")).`as`("dateHour")
                .and("event"))

        val sortOperation = sort(Direction.DESC, "dateWithHour")
        operations.add(sortOperation)

        if (limit != null) {
            operations.add(limit(limit))
        }

        val agg = newAggregation(operations)

        /*  Aggregation agg = newAggregation(matchOperation,
                projectionOperation,
                groupOperation,
                project("count")
                        .and(DateOperators.DateFromString.fromStringOf("dateWithHour")
                                .withFormat("%Y-%m-%d %H")).as("dateHour")
                        .and("event"),
                sortOperation,
                limit(36));*/

        val groupResults = mongoTemplate.aggregate(agg, EventLog::class.java, EventLogStatisticView::class.java)
        return groupResults.mappedResults.sortedBy { it.dateHour }

        /*if (list != null) {
            list = list.sortedBy { it.dateHour }

            *//*list = list.stream()
                    .sorted(Comparator.comparing<EventLogStatisticView, Date>(Function<EventLogStatisticView, Date> { it.getDateHour() }))
                    .collect<List<EventLogStatisticView>, Any>(Collectors.toList())*//*
        }*/
    }

    private fun buildCriteriaList(filterBy: EventLogFilter?): List<Criteria>? {

        if (filterBy == null) {
            return null
        }

        val criterias = ArrayList<Criteria>()

        if (filterBy.id != null) {
            criterias.add(where("_id").`is`(filterBy.id))
        }

        if (filterBy.identifier != null) {
            criterias.add(where("identifier").`is`(filterBy.identifier))
        }

        if (filterBy.componentId != null) {
            criterias.add(where("componentId").`is`(filterBy.componentId))
        }

        if (filterBy.eventId != null) {
            criterias.add(where("eventId").`is`(filterBy.eventId))
        }

        if (filterBy.eventType != null) {
            criterias.add(where("eventType").`is`(filterBy.eventType!!.name))
        }

        if (filterBy.ruleId != null) {
            criterias.add(where("ruleId").`is`(filterBy.ruleId))
        }

        if (filterBy.messageId != null) {
            criterias.add(where("messageId").`is`(filterBy.messageId))
        }

        if (filterBy.hash != null) {
            criterias.add(where("hash").`is`(filterBy.hash))
        }

        if (filterBy.dateHour != null || filterBy.dateHourEnd != null) {
            if (filterBy.dateHour != null || filterBy.dateHourEnd != null) {
                criterias.add(Util.dateToBetweenCriteria("dateHour",
                        filterBy.dateHour, filterBy.dateHourEnd))
            }
        }

        if (!filterBy.dataFields.isNullOrEmpty()) {
            filterBy.dataFields.forEach { dataFieldLog ->
                val elemMatch = Criteria()
                elemMatch.andOperator(Util.dataFieldToCriteria(dataFieldLog))

                criterias.add(where("dataFields")
                        .elemMatch(elemMatch))
            }
        }
        return criterias
    }
}
