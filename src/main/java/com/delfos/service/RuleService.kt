package com.delfos.service

import com.delfos.domain.*
import com.delfos.engine.rules.ValidationRuleManager
import com.delfos.repository.RuleRepository
import com.eoscode.springapitools.service.AbstractService
import com.eoscode.springapitools.service.exceptions.EntityNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class RuleService @Autowired
constructor(private val ruleRepository: RuleRepository,
            private val fieldService: FieldService,
            private val conditionService: ConditionService,
            private val validationRuleManager: ValidationRuleManager) : AbstractService<RuleRepository, Rule, String>() {

    override fun getRepository(): RuleRepository {
        return ruleRepository
    }

    private fun prepareRule(rule: Rule) {
        if (rule.conditions != null) {
            prepareConditions(rule, rule.conditions, null)
        }
    }
    private fun prepareConditions(rule: Rule, conditions: List<Condition>?, parent: Condition?) {
        conditions?.forEach { condition ->
            if (condition.type() === ConditionType.FIELD) {
                val dataFieldCondition = condition as MessageDataFieldCondition
                if (dataFieldCondition.field == null) {
                    dataFieldCondition.field = Field(dataFieldCondition.fieldId)
                }
            }

            if (condition.parentId != null) {
                condition.parent = Condition(condition.parentId)
            } else if (parent != null) {
                condition.parent = parent
            }

            condition.rule = rule

            if (condition.conditions != null) {
                prepareConditions(rule, condition.conditions, condition)
            }
        }
    }

    private fun singleConditionList(conditions: List<Condition>): MutableList<Condition> {
        val singleList = ArrayList<Condition>()
        conditions.forEach { condition ->
            if (condition.type() === ConditionType.GROUP) {
                condition.conditions?.let {
                    singleList.addAll(it)
                }
            }
            singleList.add(condition)
        }
        return singleList
    }

    @Throws(EntityNotFoundException::class)
    override fun findDetailById(id: String?): Rule {
        return repository.loadRule(id!!)
    }

    @Transactional
    override fun save(entity: Rule): Rule {

        if (entity.state == null) {
            entity.state = StateType.DISABLED
        }

        if (entity.id != null) {
            val ruleOld = findDetailById(entity.id)
            val conditions = entity.conditions?.let { singleConditionList(it) }
            val conditionsRemoved = ruleOld.conditions?.let { singleConditionList(it) }

            conditionsRemoved?.apply {
                removeAll(conditions.orEmpty())
                forEach { condition -> conditionService.deleteById(condition.id) }
            }
        }

        prepareRule(entity)
        val newEntity = super.save(entity)

        validationRuleManager.addRule(newEntity)
        return newEntity
    }

    override fun update(rule: Rule): Rule {
        prepareRule(rule)
        val newRule = super.update(rule)

        validationRuleManager.addRule(newRule)
        return newRule

    }

    fun findAllEnabled(): List<Rule> {
        return ruleRepository.findAllEnabled()
    }

    fun findByNameOrDescription(term: String): List<Rule> {
        return repository.findByNameLikeIgnoreCaseOrDescriptionLikeIgnoreCaseOrderByName("%$term%", term)
    }

}
