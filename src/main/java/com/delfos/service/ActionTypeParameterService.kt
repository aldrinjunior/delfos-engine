package com.delfos.service

import com.delfos.domain.ActionTypeParameter
import com.delfos.repository.ActionTypeParameterRepository
import com.eoscode.springapitools.service.AbstractService
import org.springframework.stereotype.Service

@Service
class ActionTypeParameterService : AbstractService<ActionTypeParameterRepository, ActionTypeParameter, String>() {

    private val actionTypeParameterRepository: ActionTypeParameterRepository? = null

    override fun getRepository(): ActionTypeParameterRepository? {
        return actionTypeParameterRepository
    }

}
