package com.delfos.service

import com.delfos.domain.*
import com.delfos.engine.ApplicationEngine
import com.delfos.engine.rules.ValidationRuleManager
import com.delfos.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.expression.ParseException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class DBServices {

    @Autowired
    private val messageGroupRepository: MessageGroupRepository? = null

    @Autowired
    private val messageRepository: MessageRepository? = null

    @Autowired
    private val ruleRepository: RuleRepository? = null

    @Autowired
    private val conditionRepository: ConditionRepository? = null

    @Autowired
    private val validationRuleManager: ValidationRuleManager? = null

    @Autowired
    private val eventRepository: EventRepository? = null

    @Autowired
    private val actionTypeRepository: ActionTypeRepository? = null

    @Autowired
    private val actionRepository: ActionRepository? = null

    @Autowired
    private val userRepository: UserRepository? = null

    @Autowired
    private val bCryptPasswordEncoder: BCryptPasswordEncoder? = null

    @Autowired
    private val applicationEngine: ApplicationEngine? = null

    fun initOK() {
        val user = User()
        user.name = "Administrator"
        user.username = "admin"
        user.password = bCryptPasswordEncoder!!.encode("123456")

        userRepository!!.save(user)
    }

    @Throws(ParseException::class)
    fun instantiateTestDatabase() {

        var messageGroup = MessageGroup()
        messageGroup.name = "Monitoração de componentes"
        messageGroup = messageGroupRepository!!.save(messageGroup)

        val message = Message()
        message.name = "status-component"
        message.description = "Indica o status do componente"
        message.messageGroup = messageGroup
        message.registryLog = true

        val componentNameField = Field(null, "name", 60, FieldType.STRING)
        message.addField(componentNameField)

        val componentStatusField = Field(null, "status", 1, FieldType.NUMBER)
        message.addField(componentStatusField)

        messageRepository!!.save(message)


        val rule = Rule()
        rule.name = "Componente desconectado"
        rule.state = StateType.ENABLED

        val nameCondition = MessageCondition()
        nameCondition.operator = OperatorType.EQ.value
        nameCondition.expressionType = MessageCondition.CONDITION_NAME
        nameCondition.value = "status-component"
        rule.addCondition(nameCondition)

        val statusCondition = MessageDataFieldCondition()
        statusCondition.andOr = OperatorType.AND.value
        statusCondition.operator = OperatorType.EQ.value
        statusCondition.field = componentStatusField
        statusCondition.value = "0"
        rule.addCondition(statusCondition)

        try {
            ruleRepository!!.save(rule)
        } catch (e: DataIntegrityViolationException) {
            e.printStackTrace()
        }

        val actionType = ActionType()
        actionType.name = "Default sysout"
        actionType.isSystem = true
        actionType.executable = "com.delfos.engine.action.ExecuteSysout"

        val typeParameter = ActionTypeParameter("message-default")
        actionType.addParameter(typeParameter)
        actionTypeRepository!!.save(actionType)


        var action = Action()
        action.name = "Delfos syout"
        action.actionType = actionType

        val actionParameter = ActionParameter()
        actionParameter.type = typeParameter
        actionParameter.value = "teste"
        action.addParameter(actionParameter)
        action = actionRepository!!.save(action)

        val event = Event()
        event.name = "Evento componente desconectado"
        event.type = EventType.INFORMATION
        event.state = StateType.ENABLED
        //event.setRetry(5);
        event.delay = 60
        event.addRule(rule)
        event.addAction(action)
        eventRepository!!.save(event)

        val user = User()
        user.name = "Eduardo Santos"
        user.username = "duosan@gmail.com"
        user.password = bCryptPasswordEncoder!!.encode("123456")
        userRepository!!.save(user)

        applicationEngine!!.init()

        println(applicationEngine.date)

    }

}
