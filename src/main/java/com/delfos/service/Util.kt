package com.delfos.service

import com.delfos.domain.OperatorType
import com.delfos.domain.filter.DataFieldLogFilter
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import java.util.*

object Util {

    fun dataFieldToCriteria(dataFieldLogFilter: DataFieldLogFilter): Criteria {

        var criteria = where("value")
        if (OperatorType.EQ.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.`is`(dataFieldLogFilter.value)
        } else if (OperatorType.NE.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.ne(dataFieldLogFilter.value)
        }
        if (OperatorType.GT.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.gt(dataFieldLogFilter.value!!)
        }
        if (OperatorType.GTE.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.gte(dataFieldLogFilter.value!!)
        }
        if (OperatorType.LT.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.lt(dataFieldLogFilter.value!!)
        }
        if (OperatorType.LTE.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.lte(dataFieldLogFilter.value!!)
        }
        if (OperatorType.IS_NULL.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.`is`(null)
        }
        if (OperatorType.IS_NOT_NUL.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.ne(null)
        }
        if (OperatorType.IN.value.equals(dataFieldLogFilter.operator!!, ignoreCase = true)) {
            criteria = criteria.`in`(dataFieldLogFilter.value!!)
        }

        return where("fieldId").`is`(dataFieldLogFilter.fieldId)
                .andOperator(criteria)
    }

    fun dateToBetweenCriteria(fieldName: String, dateInit: Date?, dateEnd: Date?): Criteria {
        val calendar = Calendar.getInstance()

        var dateInitWithHour = Date()
        dateInit?.let { dateInitWithHour = it }

        calendar.time = dateInitWithHour
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0)
        dateInitWithHour = calendar.time

        if (dateEnd != null) {
            calendar.time = dateEnd
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59)
            return Criteria.where(fieldName).gte(dateInitWithHour).lte(calendar.time)
        } else {
            calendar.time = dateInit
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH), 23, 59, 59)
            return Criteria.where(fieldName).gte(dateInitWithHour).lte(calendar.time)
        }
    }

}
